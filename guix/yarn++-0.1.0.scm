(define-module (yarn++)
    #:use-module (guix)
    #:use-module (guix git-download)
    #:use-module (guix build-system gnu)
    #:use-module (guix licenses)
    #:use-module (gnu packages)
    #:use-module (gnu packages gcc)
)

(define %yarnpp-git "https://gitlab.inria.fr/convergence-hpc-bigdata/hpc-hadoop-light/yarnhpc.git")

(define-public yarn++-0.1.0
    (package
        (name "Yarn++")
        (version "0.1.0")
        (source 
            (origin
                (method git-fetch)
                (uri
                    (git-reference
                        (url %yarnpp-git)
                        (commit (string-append "starpu-" version))
                    )
                )
                (file-name (string-append name "-" version "-checkout"))
                (sha256
                    (base32 "0mb8wn6pdh5z714930a034jq4vdblk50h9ya7iw7g6rs53dcv9kf")
                )
            )
        )
        (build-system gnu-build-system)
        (propagated-inputs
            `(
                ("protobuf", protobuf-2)
                ("sasl", cyrus-sasl)
            )
        )
        (synopsis "Yarn++: A C++ library to launch HPC on Hadoop clusters")
        (description "Yarn++ is a library used to launch C++ applications on a Hadoop v3.2.1 cluster developed in the context of the convergence between HPC and Big Data. It allows to create application client and application master to submit YARN applications and run C++ code on compute node.")
        (home-page "https://gitlab.inria.fr/convergence-hpc-bigdata/hpc-hadoop-light/yarnhpc")
        (license gpl3+)
    )
)