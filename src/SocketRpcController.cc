#include <Yarn++/SocketRpcController.hpp>

namespace Yarnpp {

SocketRpcController::~SocketRpcController() {}

void SocketRpcController::Reset() {

}

bool SocketRpcController::Failed() const {
    return false;
}

string SocketRpcController::ErrorText() const {
    return "";
}

void SocketRpcController::StartCancel() {

}

void SocketRpcController::SetFailed(const string& reason) {

}

bool SocketRpcController::IsCanceled() const {
    return false;
}

void SocketRpcController::NotifyOnCancel(google::protobuf::Closure* callback) {
    
}

}