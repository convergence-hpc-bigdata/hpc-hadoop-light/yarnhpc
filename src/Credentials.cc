#include <Yarn++/Credentials.hpp>

#include <iostream>
#include <cstring>

namespace Yarnpp {

Credentials::Credentials() {}

Credentials::~Credentials() {
    for (auto itr = this->secrets.begin(); itr != this->secrets.end(); ++itr) {
        delete itr->second.second;
    }
}

void Credentials::readTokenStorageFile(const char* filename) {
    ifstream ifs(filename, ios::binary | ios::in);

    if (ifs.is_open()) {
        char* magic = (char*)calloc(this->TOKEN_STORAGE_MAGIC_LEN + 1, sizeof(char));
        ifs.read(magic, this->TOKEN_STORAGE_MAGIC_LEN);

        if (strcmp(magic, this->TOKEN_STORAGE_MAGIC) != 0) {
            cerr << "Bad header found in token storage !" << endl;
        } else {
            char format;
            ifs.read(&format, 1);

            if (format == this->WRITABLE) {
                readFields(ifs);
            } else if (format == this->PROTOBUF) {
                readProto(ifs);
            } else {
                cerr << "Bad format !" << endl;
            }
        }

        delete magic;
    } else {
        cerr << "Can't open filename: " << filename << endl;
    }
}

void Credentials::readFields(ifstream& ifs) {
    int size = this->readVInt(ifs);

    for (int i = 0; i < size; ++i) {
        // Read token
        int len = this->readVInt(ifs);
        string text("");
        if (len != 0) {
            char* cText = new char[len];
            ifs.read(cText, len);
            text.append(cText);
            delete cText;
        }

        hadoop::common::TokenProto token;
        len = this->readVInt(ifs);
        char* payload;
        if (len > 0) {
            char* payload = (char*)calloc(len, sizeof(char));
            ifs.read(payload, len);
            token.set_identifier(payload);
            delete payload;
        }

        len = this->readVInt(ifs);
        if (len > 0) {
            payload = (char*)calloc(len, sizeof(char));
            ifs.read(payload, len);
            token.set_password(payload);
            delete payload;
        }

        len = this->readVInt(ifs);
        if (len > 0) {
            payload = (char*)calloc(len, sizeof(char));
            ifs.read(payload, len);
            token.set_kind(payload);
            delete payload;
        }

        len = this->readVInt(ifs);
        if (len > 0) {
            payload = (char*)calloc(len, sizeof(char));
            ifs.read(payload, len);
            token.set_service(payload);
            delete payload;
        }

        cout << "Read token: " << text << endl;
        cout << token.DebugString() << endl;
        this->tokens.insert({text, token});
    }

    size = this->readVInt(ifs);
    for (int i = 0; i < size; ++i) {
        // Read secret
        int len = this->readVInt(ifs);
        string text("");
        if (len != 0) {
            char* cText = (char*)calloc(len, sizeof(char));
            ifs.read(cText, len);
            text.append(cText);
            delete cText;
        }

        len = this->readVInt(ifs);
        char* payload = new char[len];
        ifs.read(payload, len);
        this->secrets.insert({text, {len, payload}});
    }
}

void Credentials::readProto(ifstream& ifs) {
    cerr << "Not implemented yet !" << endl;
}

// public static int decodeVIntSize(byte value) {
//     if (value >= -112) {
//         return 1;
//     } else if (value < -120) {
//         return -119 - value;
//     }
//     return -111 - value;
// }
int decodeVIntSize(char byte) {
    if (byte >= -112) {
        return 1;
    } else if (byte < -120) {
        return -119 - byte;
    }
    return -111 - byte;
}
// public static boolean isNegativeVInt(byte value) {
//     return value < -120 || (value >= -112 && value < 0);
// }
bool isNegativeVInt(char val) {
    return val < -120 || (val >= -112 && val < 0);
}
// public static long readVLong(DataInput stream) throws IOException {
//     byte firstByte = stream.readByte();
//     int len = decodeVIntSize(firstByte);
//     if (len == 1) {
//         return firstByte;
//     }
//     long i = 0;
//     for (int idx = 0; idx < len-1; idx++) {
//         byte b = stream.readByte();
//         i = i << 8;
//         i = i | (b & 0xFF);
//     }
//     return (isNegativeVInt(firstByte) ? (i ^ -1L) : i);
// }
int Credentials::readVInt(ifstream& ifs) {
    char firstByte;
    ifs.read(&firstByte, 1);

    int len = decodeVIntSize(firstByte);
    if (len == 1) {
        return firstByte;
    }
    int i = 0;
    char byte;
    for (int idx = 0; idx < len-1; idx++) {
        ifs.read(&byte, 1);
        i = i << 8;
        i = i | (byte & 0xFF);
    }
    return (isNegativeVInt(firstByte) ? (i ^ -1L) : i);
}

hadoop::common::TokenProto Credentials::getToken(string text) {
    return this->tokens[text];
}

pair<int, char*> Credentials::getSecret(string text) {
    return this->secrets[text];
}

}