/**
 * This class is a C++ adaptation of the Cerastes channel.py source code
 * The license is available here: https://github.com/yassineazzouz/cerastes/blob/master/LICENSE
 */

#include <Yarn++/RpcBufferedReader.hpp>

#include <Yarn++/Verbosity.hpp>
#include <Yarn++/Utils.hpp>
#include <sys/socket.h>
#include <iostream>
#include <cstdlib>

using namespace std;

namespace Yarnpp
{

    RpcBufferedReader::RpcBufferedReader(int sock)
    {
        this->sockfd = sock;
        this->reset();
    }

    RpcBufferedReader::~RpcBufferedReader()
    {
        free(this->buffer);
    }

    void RpcBufferedReader::reset()
    {
        free(this->buffer);
        this->bufferLength = 100;
        this->bufferRead = 0;
        this->buffer = (char *)calloc(this->bufferLength, sizeof(char));
        this->pos = 0;
    }

    void RpcBufferedReader::rewind(size_t places)
    {
        this->pos -= places;
    }

    size_t RpcBufferedReader::_buffer_bytes(size_t n)
    {
        size_t toRead = n;

        // Check if buffer is big enough
        while (this->bufferLength <= this->pos + n)
        {
            this->bufferLength *= 2; // Avoid doing a lot of realloc
            this->buffer = (char *)realloc(this->buffer, this->bufferLength * sizeof(char));
        }

        for (int i = 0; i < MAX_READ_ATTEMPTS; ++i)
        {
            size_t read = recv(this->sockfd, &(this->buffer[this->bufferRead]), toRead, 0);
            in(&(this->buffer[this->bufferRead]), read);
            this->bufferRead += read;

            toRead -= read;

            if (toRead == 0)
            {
                return n;
            }
        }

        return 0;
    }

    void RpcBufferedReader::in(const void *bytes, size_t len)
    {
        if (this->verbose & Verbose::Packets)
        {
            cerr << "- Getting " << len << " bytes" << endl;
            FormatBytes(bytes, len);
        }
    }

    void RpcBufferedReader::setVerbosity(long long verbose)
    {
        this->verbose = verbose;
    }

    char *RpcBufferedReader::read(size_t n)
    {
        // n - self.buffer_length + self.pos + 1
        long bytesWanted = n - this->bufferRead + this->pos;

        if (bytesWanted > 0)
        {
            this->_buffer_bytes(bytesWanted);
        }

        char *result = &(this->buffer[this->pos]);
        this->pos += n;

        return result;
    }

}