#include <iostream>

#ifdef WITH_HWLOC
    #include <hwloc.h>
#endif

using namespace std;

namespace Yarnpp {
    namespace hwloc {
        #ifdef WITH_HWLOC
            void print_children(hwloc_topology_t topology, hwloc_obj_t obj, int depth)
            {
                char type[32], attr[1024];
                unsigned i;

                hwloc_obj_type_snprintf(type, sizeof(type), obj, 0);
                printf("%*s%s", 2*depth, "", type);
                if (obj->os_index != (unsigned) -1)
                printf("#%u", obj->os_index);
                hwloc_obj_attr_snprintf(attr, sizeof(attr), obj, " ", 0);
                if (*attr)
                printf("(%s)", attr);
                printf("\n");
                for (i = 0; i < obj->arity; i++) {
                    print_children(topology, obj->children[i], depth + 1);
                }
            }

            void print_resources() {
                hwloc_topology_t topology;
                hwloc_cpuset_t cpuset;

                /* Allocate and initialize topology object. */
                hwloc_topology_init(&topology);

                /* Perform the topology detection. */
                hwloc_topology_load(topology);

                cerr << "+++ Resources +++" << endl;

                printf("+ Printing overall tree\n");
                print_children(topology, hwloc_get_root_obj(topology), 0);

                cerr << "+++ End of resources +++" << endl;

                /* Destroy topology object. */
                hwloc_topology_destroy(topology);
            }
        #else
            void print_resources() {
                cerr << "Logging resources without HWLOC !" << endl;
            }
        #endif
    }
}
