#include <Yarn++/ContainerManager.hpp>

#include <Yarn++/Utils.hpp>
#include <iostream>

// Protobuf
#include <Yarn++/genprotos/yarn_service_protos.pb.h>
#include <Yarn++/genprotos/yarn_security_tokens.pb.h>

extern char **environ;

namespace Yarnpp {
    ContainerManager::ContainerManager(string host, int port, hadoop::common::TokenProto nodeManagerToken, hadoop::common::TokenProto containerToken, string effectiveUser, int verbosity) {
        this->effectiveUser = effectiveUser;
        this->username = "";
        this->password = "";
        this->nodeManagerToken = nodeManagerToken;
        this->containerToken = containerToken;

        this->parseToken();

        if (this->username == "" || this->password == "" || this->effectiveUser == "") {
            cerr << "CM: Error while parsing tokens!"  << endl;
            exit(-2);
        }

        this->channel = new SocketRpcChannel(
            host, port,
            this->DEFAULT_YARN_PROTOCOL_VERSION,
            "org.apache.hadoop.yarn.api.ContainerManagementProtocolPB",
            this->effectiveUser,
            true
        );
        this->channel->setVerbosity(verbosity);
        this->channel->setUsername(this->username.c_str());
        this->channel->setPassword(this->password.c_str());

        this->controller = new SocketRpcController();
        this->service = new hadoop::yarn::ContainerManagementProtocolService::Stub(channel);
    }

    ContainerManager::~ContainerManager() {
        delete this->service;
        delete this->controller;
        delete this->channel;
    }

    void ContainerManager::startContainer(string command) {
        hadoop::yarn::StartContainersRequestProto* request = new hadoop::yarn::StartContainersRequestProto;
        hadoop::yarn::StartContainerRequestProto* context = request->add_start_container_request();

        // Copy container token
        hadoop::common::TokenProto* containerTokenProto = new hadoop::common::TokenProto;
        containerTokenProto->ParseFromString(this->containerToken.SerializeAsString());

        // Print both token
        printProtobufMessage(&this->nodeManagerToken);
        printProtobufMessage(containerTokenProto);

        context->set_allocated_container_token(containerTokenProto);

        hadoop::yarn::ContainerLaunchContextProto* launchContext = new hadoop::yarn::ContainerLaunchContextProto;
        launchContext->add_command(command.c_str());

        context->set_allocated_container_launch_context(launchContext);

        hadoop::yarn::StartContainersResponseProto response;

        this->service->startContainers(this->controller, request, &response, NULL);

        cerr << "Printing response: " << endl;
        printProtobufMessage(&response);
    }

    void ContainerManager::stopContainer() {

    }

    void ContainerManager::parseToken() {
        this->username = base64_encode(nodeManagerToken.identifier());
        this->password = base64_encode(nodeManagerToken.password());
    }
}