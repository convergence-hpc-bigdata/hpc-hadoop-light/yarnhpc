/**
 * This class is a C++ adaptation of the Cerastes channel.py source code
 * The license is available here: https://github.com/yassineazzouz/cerastes/blob/master/LICENSE
 */

#include <Yarn++/SocketRpcChannel.hpp>

#include <google/protobuf/io/coded_stream.h>
#include <Yarn++/Verbosity.hpp>
#include <Yarn++/Utils.hpp>
#include <netinet/tcp.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <iostream>
#include <unistd.h>
#include <fcntl.h>
#include<netdb.h>

/* Protobufs */
#include <Yarn++/genprotos/IpcConnectionContext.pb.h>
#include <Yarn++/genprotos/ProtobufRpcEngine.pb.h>

/* Python-SASL */
#include <sasl/saslwrapper.h>

namespace Yarnpp
{

    struct protobufDelimitedMessage
    {
        size_t len;
        char *bytes;
    };

    void getDelimitedMessageBytes(RpcBufferedReader *stream, protobufDelimitedMessage *msg, int nr = 4)
    {
        const char *read = stream->read(nr);
        const char *oldStream = read;
        google::protobuf::uint32 length = ReadVarint32(&read);
        size_t pos = read - oldStream;

        size_t delimiterBytes = nr - pos;
        stream->rewind(delimiterBytes);
        char *messageBytes = stream->read(length);

        char *bytes = (char *)calloc(length + 1, sizeof(char));
        memcpy(bytes, messageBytes, length);
        msg->len = length + pos;
        msg->bytes = bytes;
    }

    SocketRpcChannel::SocketRpcChannel(string host, int port, char version, string contextProcotol, string effectiveUser, bool useSasl, bool krbPrincipal, int sockConnectTimeout, int sockRequestTimeout)
    {
        // Get ip from host
        struct hostent* he;
        struct in_addr** addr_list;

        if ((he = gethostbyname(host.c_str())) == NULL) {
            cerr << "Hostname not known: " << host << endl;
            exit(1);
        }

        addr_list = (struct in_addr**)he->h_addr_list;
        this->host = inet_ntoa(*addr_list[0]);
        cerr << "Hostname: " << this->host << endl;

        this->port = port;
        this->sock = 0;
        this->callID = -3;
        this->version = version;
        this->clientID = "toto";
        this->useSasl = useSasl;
        this->contextProtocol = contextProcotol;

        if (useSasl)
        {
            cerr << "! Kerberos not supported yet !" << endl;
            this->effectiveUser = effectiveUser;
        }
        else
        {
            this->effectiveUser = effectiveUser;
        }

        this->sockConnectTimeout = sockConnectTimeout;
        this->sockRequestTimeout = sockRequestTimeout;

        this->username = NULL;
        this->password = NULL;
    }

    SocketRpcChannel::~SocketRpcChannel()
    {
    }

    void SocketRpcChannel::setVerbosity(long long level)
    {
        this->verbose = level;
    }

    void SocketRpcChannel::setUsername(const char *username)
    {
        if (this->username != NULL)
        {
            free(this->username);
        }
        this->username = (char *)calloc(strlen(username), sizeof(char));
        strcpy(this->username, username);
    }

    void SocketRpcChannel::setPassword(const char *password)
    {
        if (this->password != NULL)
        {
            free(this->password);
        }
        this->password = (char *)calloc(strlen(password), sizeof(char));
        strcpy(this->password, password);
    }

    void SocketRpcChannel::validateRequest(const google::protobuf::Message *request)
    {
        if (!request->IsInitialized())
        {
            cerr << "! Client request (" << request->GetTypeName() << ") is missing mandatory fields" << endl;
        }
    }

    void SocketRpcChannel::getConnection()
    {
        /** Open a socket connection to a given host and port and writes the Hadoop header
        The Hadoop RPC protocol looks like this when creating a connection:
        +---------------------------------------------------------------------+
        |  Header, 4 bytes ("hrpc")                                           |
        +---------------------------------------------------------------------+
        |  Version, 1 byte (default verion 9)                                 |
        +---------------------------------------------------------------------+
        |  RPC service class, 1 byte (0x00)                                   |
        +---------------------------------------------------------------------+
        |  Auth protocol, 1 byte (Auth method None = 0)                       |
        +---------------------------------------------------------------------+
        |  Length of the RpcRequestHeaderProto  + length of the               |
        |  of the IpcConnectionContextProto (4 bytes/32 bit int)              |
        +---------------------------------------------------------------------+
        |  Serialized delimited RpcRequestHeaderProto                         |
        +---------------------------------------------------------------------+
        |  Serialized delimited IpcConnectionContextProto                     |
        +---------------------------------------------------------------------+
        **/

        // Open socket
        if ((this->sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        {
            cerr << "! Socket creation error !" << endl;
            return;
        }

        int yes = 1;
        setsockopt(this->sock, IPPROTO_TCP, TCP_NODELAY, &yes, sizeof(int));

        struct sockaddr_in serv_addr = {
            .sin_family = AF_INET,
            .sin_port = htons(this->port)
        };

        if (inet_pton(AF_INET, this->host.c_str(), &serv_addr.sin_addr) <= 0)
        {
            cerr << "! Invalid address / Address not supported (" << this->host << ") !" << endl;
            return;
        }

        // Set the connection timeout
        long arg;
        if ((arg = fcntl(this->sock, F_GETFL, NULL)) < 0)
        {
            cerr << "! Error with fcntl: " << strerror(errno) << endl;
            return;
        }
        arg |= O_NONBLOCK;
        if (fcntl(this->sock, F_SETFL, arg) < 0)
        {
            cerr << "! Error while setting non blocking flag: " << strerror(errno) << endl;
            return;
        }

        arg = connect(this->sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr));
        if (arg < 0)
        {
            if (errno == EINPROGRESS)
            {
                struct timeval tv;
                fd_set myset;
                do
                {
                    tv.tv_sec = sockConnectTimeout / 1000;
                    tv.tv_usec = 0;
                    FD_ZERO(&myset);
                    FD_SET(this->sock, &myset);

                    arg = select(this->sock + 1, NULL, &myset, NULL, &tv);
                    if (arg < 0 && errno != EINTR)
                    {
                        cerr << "! Error connecting: " << strerror(errno) << endl;
                        return;
                    }
                    else if (arg > 0)
                    {
                        int valopt;
                        socklen_t lon = sizeof(int);
                        if (getsockopt(this->sock, SOL_SOCKET, SO_ERROR, (void *)(&valopt), &lon) < 0)
                        {
                            cerr << "! Error in getsockopt: " << strerror(errno) << endl;
                        }

                        if (valopt)
                        {
                            cerr << "! Error in delayed connection: " << strerror(valopt) << endl;
                        }
                        break;
                    }
                    else
                    {
                        cerr << "! Connection timeout" << endl;
                    }
                } while (1);
            }
            else
            {
                cerr << "! Error while connecting: " << strerror(errno) << endl;
            }
        }

        // Set blocking again
        if ((arg = fcntl(this->sock, F_GETFL, NULL)) < 0)
        {
            cerr << "! Error with fcntl: " << strerror(errno) << endl;
            return;
        }
        arg &= (~O_NONBLOCK);
        if (fcntl(this->sock, F_SETFL, arg) < 0)
        {
            cerr << "! Error while setting blocking flag: " << strerror(errno) << endl;
            return;
        }

        // Send RPC headers
        this->write(this->RPC_HEADER.c_str(), this->RPC_HEADER.length());
        this->write(&this->version, sizeof(char));
        this->write(&this->RPC_SERVICE_CLASS, sizeof(char));
        if (this->useSasl)
        {
            this->write(&this->AUTH_PROTOCOL_SASL, sizeof(char));
        }
        else
        {
            this->write(&this->AUTH_PROTOCOL_NONE, sizeof(char));
        }

        if (this->useSasl)
        {
            hadoop::common::RpcSaslProto negotiate;
            negotiate.set_state(hadoop::common::RpcSaslProto_SaslState::RpcSaslProto_SaslState_NEGOTIATE);

            this->sendSaslMessage(&negotiate);

            saslwrapper::ClientImpl saslClient;
            string saslError;
            saslClient.setAttr("service", "");
            saslClient.setAttr("host", "default");

            if (this->username == NULL || this->password == NULL)
            {
                cerr << "! Error: no username or password" << endl;
                exit(0);
            }

            saslClient.setAttr("username", this->username);
            saslClient.setAttr("password", this->password);
            if (!saslClient.init())
            {
                saslClient.getError(saslError);
                cerr << "! Error during SASL init: " << saslError << endl;
                exit(-1);
            }

            while (true)
            {
                hadoop::common::RpcSaslProto response = this->recvSaslMessage();
                if (response.state() == hadoop::common::RpcSaslProto_SaslState::RpcSaslProto_SaslState_NEGOTIATE)
                {
                    string mechs = "";
                    for (auto it = response.auths().begin(); it != response.auths().end(); ++it)
                    {
                        mechs.append((*it).mechanism());
                        mechs.append(",");
                    }

                    if (this->verbose & Verbose::Packets)
                    {
                        cerr << "* Sasl mechanisms: (" << mechs << ")" << endl;
                    }

                    string chosen;
                    string initialResponse;
                    if (!saslClient.start(mechs, chosen, initialResponse))
                    {
                        saslClient.getError(saslError);
                        cerr << "! Error during SASL start: " << saslError << endl;
                        exit(-1);
                    }

                    hadoop::common::RpcSaslProto initiate;
                    initiate.set_state(hadoop::common::RpcSaslProto_SaslState::RpcSaslProto_SaslState_INITIATE);
                    initiate.set_token(initialResponse.c_str());

                    string challenge("");

                    for (auto it = response.auths().begin(); it != response.auths().end(); ++it)
                    {
                        if ((*it).mechanism().compare(chosen.c_str()) == 0)
                        {
                            hadoop::common::RpcSaslProto_SaslAuth *auth = initiate.add_auths();
                            auth->set_mechanism((*it).mechanism());
                            auth->set_method((*it).method());
                            auth->set_protocol((*it).protocol());
                            auth->set_serverid((*it).serverid());

                            if ((*it).has_challenge())
                            {
                                challenge = (*it).challenge();
                            }
                        }
                    }

                    if (challenge.length() > 0)
                    {
                        string resToken;
                        saslClient.step(challenge, resToken);
                        initiate.set_token(resToken.c_str());
                    }

                    this->sendSaslMessage(&initiate);
                    continue;
                }

                if (response.state() == hadoop::common::RpcSaslProto_SaslState::RpcSaslProto_SaslState_CHALLENGE)
                {
                    string resToken;
                    if (!saslClient.step(response.token(), resToken))
                    {
                        saslClient.getError(saslError);
                        cerr << "! Error during sasl step: " << saslError << endl;
                        exit(-1);
                    }

                    hadoop::common::RpcSaslProto responseChallenge;
                    responseChallenge.set_token(resToken.c_str());
                    responseChallenge.set_state(hadoop::common::RpcSaslProto_SaslState::RpcSaslProto_SaslState_RESPONSE);
                    this->sendSaslMessage(&responseChallenge);
                    continue;
                }

                if (response.state() == hadoop::common::RpcSaslProto_SaslState::RpcSaslProto_SaslState_SUCCESS)
                {
                    break;
                }
            }
        }

        string header = this->createRpcRequestHeader();
        string context = this->createConnectionContext();

        int headerLength = header.length() + google::protobuf::io::CodedOutputStream::VarintSize32(header.length()) + context.length() + google::protobuf::io::CodedOutputStream::VarintSize32(context.length());

        int headerLengthNetworkEndian = htonl(headerLength);

        this->write(&headerLengthNetworkEndian, sizeof(int));
        this->writeDelimited(header.c_str(), header.length());
        this->writeDelimited(context.c_str(), context.length());
    }

    void SocketRpcChannel::connectSasl()
    {
        hadoop::common::RpcSaslProto negotiate;
        negotiate.set_state(hadoop::common::RpcSaslProto_SaslState::RpcSaslProto_SaslState_NEGOTIATE);

        this->sendSaslMessage(&negotiate);
    }

    void SocketRpcChannel::write(const void *data, size_t len)
    {
        out(data, len);
        send(this->sock, data, len, 0);
    }

    char *_VarintBytes(size_t value)
    {
        char *result = (char *)calloc(sizeof(value), sizeof(char));
        int counter = 0;

        size_t bits = value & 0x7f;
        value >>= 7;
        while (value)
        {
            result[counter++] = 0x80 | bits;
            bits = value & 0x7f;
            value >>= 7;
        }
        result[counter] = bits;

        return result;
    }

    void SocketRpcChannel::writeDelimited(const void *data, size_t len)
    {
        char *varint = _VarintBytes(len);
        if (len == 0)
        {
            *varint = 0;
            this->write(varint, sizeof(char));
        }
        else
        {
            this->write(varint, strlen(varint));
        }
        free(varint);
        this->write(data, len);
    }

    string SocketRpcChannel::createRpcRequestHeader()
    {
        hadoop::common::RpcRequestHeaderProto header;
        header.set_rpckind(hadoop::common::RpcKindProto::RPC_PROTOCOL_BUFFER);
        header.set_rpcop(hadoop::common::RpcRequestHeaderProto_OperationProto::RpcRequestHeaderProto_OperationProto_RPC_FINAL_PACKET);
        header.set_callid(this->callID);
        header.set_retrycount(-1);
        header.set_clientid(this->clientID);

        if (this->callID == -3)
        {
            this->callID = 0;
        }
        else
        {
            this->callID += 1;
        }

        // Serialize
        string serialized = header.SerializeAsString();
        return serialized;
    }

    string SocketRpcChannel::createConnectionContext()
    {
        hadoop::common::IpcConnectionContextProto context;

        context.mutable_userinfo()->set_effectiveuser(this->effectiveUser);
        context.set_protocol(this->contextProtocol);

        string serialized = context.SerializeAsString();
        return serialized;
    }

    void SocketRpcChannel::sendRpcMessage(const google::protobuf::MethodDescriptor *method, const google::protobuf::Message *request)
    {
        /**
        Sends a Hadoop RPC request to the NameNode.
        The IpcConnectionContextProto, RpcPayloadHeaderProto and HadoopRpcRequestProto
        should already be serialized in the right way (delimited or not) before
        they are passed in this method.
        The Hadoop RPC protocol looks like this for sending requests:
        When sending requests
        +---------------------------------------------------------------------+
        |  Length of the next three parts (4 bytes/32 bit int)                |
        +---------------------------------------------------------------------+
        |  Delimited serialized RpcRequestHeaderProto (varint len + header)   |
        +---------------------------------------------------------------------+
        |  Delimited serialized RequestHeaderProto (varint len + header)      |
        +---------------------------------------------------------------------+
        |  Delimited serialized Request (varint len + request)                |
        +---------------------------------------------------------------------+
    **/
        if (this->verbose & Verbose::Packets)
        {
            cerr << "--> Sending rpc call: " << method->name() << endl;
        }

        // 0. RpcRequestHeaderProto
        string rpcheader = this->createRpcRequestHeader();

        // 1. RequestHeaderProto
        string header = this->createRequestHeader(method);

        // 2. Param
        string param = request->SerializeAsString();

        int rpcMessageLength = rpcheader.length() + google::protobuf::io::CodedOutputStream::VarintSize32(rpcheader.length()) + header.length() + google::protobuf::io::CodedOutputStream::VarintSize32(header.length()) + param.length() + google::protobuf::io::CodedOutputStream::VarintSize32(param.length());

        int rpcMessageLengthNetworkEndian = htonl(rpcMessageLength);
        this->write(&rpcMessageLengthNetworkEndian, sizeof(int));

        this->writeDelimited(rpcheader.c_str(), rpcheader.length());
        this->writeDelimited(header.c_str(), header.length());
        this->writeDelimited(param.c_str(), param.length());
    }

    void SocketRpcChannel::sendSaslMessage(const google::protobuf::Message *msg)
    {
        hadoop::common::RpcRequestHeaderProto rpcHeader;
        rpcHeader.set_rpckind(hadoop::common::RpcKindProto::RPC_PROTOCOL_BUFFER);
        rpcHeader.set_rpcop(hadoop::common::RpcRequestHeaderProto_OperationProto::RpcRequestHeaderProto_OperationProto_RPC_FINAL_PACKET);
        rpcHeader.set_callid(-33); // SASL
        rpcHeader.set_retrycount(-1);
        rpcHeader.set_clientid("toto");

        string rpcHeaderString = rpcHeader.SerializeAsString();
        string messageString = msg->SerializeAsString();

        int length = rpcHeaderString.length() + google::protobuf::io::CodedOutputStream::VarintSize32(rpcHeaderString.length()) + messageString.length() + google::protobuf::io::CodedOutputStream::VarintSize32(messageString.length());
        int networkLength = htonl(length);

        this->write(&networkLength, sizeof(int));
        this->writeDelimited(rpcHeaderString.c_str(), rpcHeaderString.length());
        this->writeDelimited(messageString.c_str(), messageString.length());
    }

    string SocketRpcChannel::createRequestHeader(const google::protobuf::MethodDescriptor *method)
    {
        hadoop::common::RequestHeaderProto header;
        header.set_methodname(method->name());
        header.set_declaringclassprotocolname(this->contextProtocol);
        header.set_clientprotocolversion(1);

        string serialized = header.SerializeAsString();
        return serialized;
    }

    RpcBufferedReader SocketRpcChannel::recvRpcMessage()
    {
        RpcBufferedReader stream = RpcBufferedReader(this->sock);
        stream.setVerbosity(this->verbose);
        return stream;
    }

    hadoop::common::RpcSaslProto SocketRpcChannel::recvSaslMessage()
    {
        hadoop::common::RpcSaslProto response;

        RpcBufferedReader stream = this->recvRpcMessage();
        this->parseResponse(&stream, &response);

        return response;
    }

    int SocketRpcChannel::getLength(RpcBufferedReader *stream)
    {
        char *lengthChar = stream->read(4);
        int length = ntohl(*((uint32_t *)lengthChar));
        return length;
    }

    int SocketRpcChannel::parseResponse(RpcBufferedReader *stream, google::protobuf::Message *responseClass)
    {
        /**
        Parses a Hadoop RPC response.
        The RpcResponseHeaderProto contains a status field that marks SUCCESS or ERROR.
        The Hadoop RPC protocol looks like the diagram below for receiving SUCCESS requests.
        +-----------------------------------------------------------+
        |  Length of the RPC resonse (4 bytes/32 bit int)           |
        +-----------------------------------------------------------+
        |  Delimited serialized RpcResponseHeaderProto              |
        +-----------------------------------------------------------+
        |  Serialized delimited RPC response                        |
        +-----------------------------------------------------------+
        In case of an error, the header status is set to ERROR and the error fields are set.
    **/

        if (this->verbose & Verbose::Packets)
        {
            cerr << "<-- Getting response" << endl;
        }

        // Read first 4 bytes to get the total length
        int totalLength = getLength(stream);

        hadoop::common::RpcResponseHeaderProto header;
        protobufDelimitedMessage msg;
        getDelimitedMessageBytes(stream, &msg);

        const string bytes((char *)msg.bytes);
        header.ParseFromString(bytes);
        int headerLength = msg.len;

        free(msg.bytes);

        if (header.status() == 0)
        {
            if (headerLength >= totalLength)
            {
                return 0;
            }

            if (totalLength - headerLength < 4)
            {
                getDelimitedMessageBytes(stream, &msg, totalLength - msg.len);
            }
            else
            {
                getDelimitedMessageBytes(stream, &msg);
            }

            // const string response((char*)msg.bytes);
            // cerr << "Length: " << msg.len << endl;
            // responseClass->ParseFromString(response);
            responseClass->ParseFromArray(msg.bytes, msg.len);

            free(msg.bytes);
            return 0;
        }
        else
        {
            this->handleError(header);
            return 1;
        }
    }

    void SocketRpcChannel::handleError(hadoop::common::RpcResponseHeaderProto header)
    {
        cerr << "! Error while parsing response: " << endl;
        cerr << "= " << header.exceptionclassname() << "=" << endl;
        cerr << header.errormsg() << endl;
    }

    void SocketRpcChannel::closeSocket()
    {
        if (this->sock)
        {
            close(this->sock);
            this->sock = 0;
        }
    }

    void SocketRpcChannel::CallMethod(const google::protobuf::MethodDescriptor *method, google::protobuf::RpcController *controller, const google::protobuf::Message *request, google::protobuf::Message *response, google::protobuf::Closure *done)
    {
        this->validateRequest(request);

        if (!this->sock)
        {
            this->getConnection();
        }

        try
        {
            this->sendRpcMessage(method, request);

            RpcBufferedReader stream = this->recvRpcMessage();

            int error = this->parseResponse(&stream, response);

            if (error == 0)
            {
                delete request;
            }
        }
        catch (const std::exception &ex)
        {
            cerr << "Exception occured: " << ex.what() << endl;
        }
    }

    void SocketRpcChannel::out(const void *bytes, size_t len)
    {
        if (this->verbose & Verbose::Packets)
        {
            cerr << "- Sending " << len << " bytes" << endl;
            FormatBytes(bytes, len);
        }
    }

}