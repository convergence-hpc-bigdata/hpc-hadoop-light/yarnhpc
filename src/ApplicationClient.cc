#include <Yarn++/ApplicationClient.hpp>

#include <Yarn++/options/hwloc.hpp>
#include <Yarn++/Verbosity.hpp>
#include <iostream>

using namespace std;

namespace Yarnpp
{
    ApplicationClient::ApplicationClient(string host, int port, string user, int verbosity)
    {
        this->channel = new SocketRpcChannel(host, port, this->DEFAULT_YARN_PROTOCOL_VERSION, "org.apache.hadoop.yarn.api.ApplicationClientProtocolPB", user);
        this->channel->setVerbosity(verbosity);

        this->controller = new SocketRpcController();

        this->service = new hadoop::yarn::ApplicationClientProtocolService::Stub(channel);

        this->hasErrorSet = false;
        this->error = "";

        /* Show platform details */
        if (verbosity & Verbose::Resources) {
            hwloc::print_resources();
        }
    }

    ApplicationClient::~ApplicationClient()
    {
        delete this->service;
        delete this->controller;
        delete this->channel;
    }

    int32_t ApplicationClient::createApplication()
    {
        hadoop::yarn::GetNewApplicationResponseProto response;
        hadoop::yarn::GetNewApplicationRequestProto *request = new hadoop::yarn::GetNewApplicationRequestProto;

        service->getNewApplication(this->controller, request, &response, NULL);

        if (!response.has_application_id())
        {
            this->putError("Error while getting new application ID !");
            return -1;
        }

        hadoop::yarn::ApplicationIdProto appId = response.application_id();

        if (!appId.has_id())
        {
            this->putError("Got response when creating new application but application ID is not set !");
            return -1;
        }

        google::protobuf::int32 id = appId.id();

        // Put new application on the map
        this->apps.insert({id, response});

        return id;
    }

    Resources ApplicationClient::getAssociatedCapabilities(int32_t appId)
    {
        // Check if application exists
        if (this->apps.find(appId) == this->apps.end())
        {
            this->putError("No application with this ID !");
            return Resources();
        }

        // Check if there is capabilities associated to it
        if (!this->apps[appId].has_maximumcapability())
        {
            this->putError("No capabilities associated with this ID !");
            return Resources();
        }

        return this->apps[appId].maximumcapability();
    }

    void ApplicationClient::submitApplication(int32_t appId, string name, int64_t memory, int32_t vcores, string appMasterCommand, vector<pair<string, string>> envVars)
    {
        // Check if application exists
        if (this->apps.find(appId) == this->apps.end())
        {
            this->putError("No application with this ID !");
            return;
        }

        hadoop::yarn::ApplicationSubmissionContextProto *context = new hadoop::yarn::ApplicationSubmissionContextProto;

        // Put app ID
        hadoop::yarn::ApplicationIdProto *appIdProto = new hadoop::yarn::ApplicationIdProto;
        hadoop::yarn::ApplicationIdProto associated = this->apps[appId].application_id();
        appIdProto->ParseFromString(associated.SerializeAsString());
        context->set_allocated_application_id(appIdProto);

        // Set name
        context->set_application_name(name);

        // Set resources
        hadoop::yarn::ResourceProto *appResources = new hadoop::yarn::ResourceProto;
        appResources->set_memory(memory);
        appResources->set_virtual_cores(vcores);
        context->set_allocated_resource(appResources);

        // Set container opts
        hadoop::yarn::ContainerLaunchContextProto *appContainer = new hadoop::yarn::ContainerLaunchContextProto;
        appContainer->add_command(appMasterCommand);

        // Set environment vars
        for (auto itr = envVars.begin(); itr != envVars.end(); ++itr)
        {
            hadoop::yarn::StringStringMapProto *env = appContainer->add_environment();
            env->set_key((*itr).first);
            env->set_value((*itr).second);
        }

        context->set_allocated_am_container_spec(appContainer);

        // Craft request
        hadoop::yarn::SubmitApplicationRequestProto *request = new hadoop::yarn::SubmitApplicationRequestProto;
        request->set_allocated_application_submission_context(context);

        hadoop::yarn::SubmitApplicationResponseProto response;

        this->service->submitApplication(this->controller, request, &response, NULL);
    }

    int32_t ApplicationClient::changeApplicationPriority(int32_t appId, int32_t newPriority)
    {
        // Check if application exist
        if (this->apps.find(appId) == this->apps.end())
        {
            this->putError("No application with this ID !");
            return -1;
        }

        // Craft request
        hadoop::yarn::UpdateApplicationPriorityRequestProto *request = new hadoop::yarn::UpdateApplicationPriorityRequestProto;
        hadoop::yarn::ApplicationIdProto *appIdProto = new hadoop::yarn::ApplicationIdProto;
        hadoop::yarn::ApplicationIdProto associated = this->apps[appId].application_id();
        appIdProto->ParseFromString(associated.SerializeAsString());

        hadoop::yarn::PriorityProto *priorityProto = new hadoop::yarn::PriorityProto;
        priorityProto->set_priority(newPriority);

        request->set_allocated_applicationid(appIdProto);
        request->set_allocated_applicationpriority(priorityProto);

        hadoop::yarn::UpdateApplicationPriorityResponseProto response;

        this->service->updateApplicationPriority(this->controller, request, &response, NULL);

        delete appIdProto;
        delete priorityProto;

        if (!response.has_applicationpriority())
        {
            this->putError("Error while changing priority !");
            return -1;
        }

        if (!response.applicationpriority().has_priority())
        {
            this->putError("Received priority change response but no priority was set !");
            return -1;
        }

        return response.applicationpriority().priority();
    }

    ApplicationReport ApplicationClient::getReport(int32_t appId)
    {
        // Check if application exist
        if (this->apps.find(appId) == this->apps.end())
        {
            this->putError("No application with this ID !");
            return ApplicationReport();
        }

        // Craft request
        hadoop::yarn::GetApplicationReportRequestProto *request = new hadoop::yarn::GetApplicationReportRequestProto;
        hadoop::yarn::ApplicationIdProto *appIdProto = new hadoop::yarn::ApplicationIdProto;
        hadoop::yarn::ApplicationIdProto associated = this->apps[appId].application_id();
        appIdProto->ParseFromString(associated.SerializeAsString());

        request->set_allocated_application_id(appIdProto);

        hadoop::yarn::GetApplicationReportResponseProto response;

        this->service->getApplicationReport(this->controller, request, &response, NULL);

        if (!response.has_application_report())
        {
            this->putError("Received response but no report was attached !");
            return ApplicationReport();
        }

        return response.application_report();
    }

    void ApplicationClient::printReport(int32_t appId)
    {
        this->clearError();
        ApplicationReport report = this->getReport(appId);

        if (this->hasErrorSet)
        {
            return;
        }

        cout << "******************" << endl;
        cout << "* App: " << appId << endl;
        cout << "******************" << endl;
        cout << "- User: " << report.user() << endl;
        cout << "- Queue: " << report.queue() << endl;
        cout << "- Name: " << report.name() << endl;
        cout << "- Host: " << report.host() << endl;
        cout << "- Port: " << report.rpc_port() << endl;
        cout << "- URL: " << report.trackingurl() << endl;
        cout << "- Start time: " << report.starttime() << endl;
        cout << "- Finish time: " << report.finishtime() << endl;
        cout << "- Progress: " << report.progress() << endl;
        cout << "- Type: " << report.applicationtype() << endl;
        cout << "- Tags: ";
        for (auto itr = report.applicationtags().begin(); itr != report.applicationtags().end(); ++itr)
        {
            cout << (*itr) << " ";
        }
        cout << endl;
        cout << "- Priority: " << report.priority().priority() << endl;
        cout << "- Tokens: " << endl;
        cout << "  - Client<->AM:" << endl;
        cout << "    - ID: " << report.client_to_am_token().identifier() << endl;
        cout << "    - Password: " << report.client_to_am_token().password() << endl;
        cout << "    - Kind: " << report.client_to_am_token().kind() << endl;
        cout << "    - Service: " << report.client_to_am_token().service() << endl;
        cout << "  - AM<->RM:" << endl;
        cout << "    - ID: " << report.am_rm_token().identifier() << endl;
        cout << "    - Password: " << report.am_rm_token().password() << endl;
        cout << "    - Kind: " << report.am_rm_token().kind() << endl;
        cout << "    - Service: " << report.am_rm_token().service() << endl;
        cout << "- Yarn application state: " << hadoop::yarn::YarnApplicationStateProto_Name(report.yarn_application_state()) << endl;
        cout << "- Final application status: " << hadoop::yarn::FinalApplicationStatusProto_Name(report.final_application_status()) << endl;
    }

    bool ApplicationClient::deleteApplication(int32_t appId)
    {
        // Check if application exist
        if (this->apps.find(appId) == this->apps.end())
        {
            this->putError("No application with this ID !");
            return false;
        }

        // Craft request
        hadoop::yarn::KillApplicationRequestProto *request = new hadoop::yarn::KillApplicationRequestProto;
        hadoop::yarn::ApplicationIdProto *appIdProto = new hadoop::yarn::ApplicationIdProto;
        hadoop::yarn::ApplicationIdProto associated = this->apps[appId].application_id();
        appIdProto->ParseFromString(associated.SerializeAsString());

        request->set_allocated_application_id(appIdProto);

        hadoop::yarn::KillApplicationResponseProto response;

        this->service->forceKillApplication(this->controller, request, &response, NULL);

        if (!response.has_is_kill_completed())
        {
            this->putError("Received response but no kill complete boolean attached !");
            return false;
        }

        if (response.is_kill_completed())
        {
            this->apps.erase(appId);
        }

        return response.is_kill_completed();
    }

    void ApplicationClient::printStatus()
    {
        cout << "**********" << endl;
        cout << "* STATUS *" << endl;
        cout << "**********" << endl;
        cout << "* " << this->apps.size() << " applications" << endl;
        cout << endl;
        for (auto itr = this->apps.begin(); itr != this->apps.end(); ++itr)
        {
            this->printReport((*itr).first);
        }
    }

    bool ApplicationClient::hasError()
    {
        return this->hasErrorSet;
    }

    string ApplicationClient::getError()
    {
        return this->error;
    }

    void ApplicationClient::clearError()
    {
        this->hasErrorSet = false;
    }

    void ApplicationClient::putError(string err)
    {
        this->hasErrorSet = true;
        this->error = err;
    }

}