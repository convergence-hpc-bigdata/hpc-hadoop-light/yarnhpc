#include <Yarn++/ApplicationMaster.hpp>

#include <Yarn++/options/hwloc.hpp>
#include <Yarn++/Verbosity.hpp>
#include <Yarn++/Utils.hpp>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <unistd.h>

// Protobuf
#include <Yarn++/genprotos/yarn_security_tokens.pb.h>

namespace Yarnpp {

ApplicationMaster::ApplicationMaster(string host, int port, int verbosity) {
    this->clearError();
    this->username = "";
    this->password = "";
    this->effectiveUser = "";
    this->verbosity = verbosity;

    this->parseTokenFile();

    if (this->hasError) {
        cerr << this->error << endl;
        exit(-1);
    }

    if (this->username == "" || this->password == "" || this->effectiveUser == "") {
        cerr << "AM: Error while parsing tokens!"  << endl;
        exit(-2);
    }

    this->channel = new SocketRpcChannel(
        host, port,
        this->DEFAULT_YARN_PROTOCOL_VERSION,
        "org.apache.hadoop.yarn.api.ApplicationMasterProtocolPB",
        this->effectiveUser,
        true
    );
    this->channel->setVerbosity(verbosity);
    this->channel->setUsername(this->username.c_str());
    this->channel->setPassword(this->password.c_str());

    this->controller = new SocketRpcController();
    this->service = new hadoop::yarn::ApplicationMasterProtocolService::Stub(channel);

    /* Show platform details */
    if (verbosity & Verbose::Resources) {
        hwloc::print_resources();
    }
}

ApplicationMaster::~ApplicationMaster() {
    delete this->service;
    delete this->controller;
    delete this->channel;
}

void ApplicationMaster::registerApplicationMaster() {
    hadoop::yarn::RegisterApplicationMasterRequestProto* request = new hadoop::yarn::RegisterApplicationMasterRequestProto;
    hadoop::yarn::RegisterApplicationMasterResponseProto response;

    this->service->registerApplicationMaster(this->controller, request, &response, NULL);
}

void ApplicationMaster::allocate(int64_t memory, int32_t vcores, string command) {
    int responseId = 0;
    bool completed = false;

    while (!completed) {
        hadoop::yarn::AllocateRequestProto *context = new hadoop::yarn::AllocateRequestProto;
        context->set_response_id(responseId);
        responseId += 1;

        // Set resources
        hadoop::yarn::ResourceRequestProto *appResourcesRequest = context->add_ask();
        appResourcesRequest->set_resource_name("*");

        hadoop::yarn::PriorityProto *priority = new hadoop::yarn::PriorityProto;
        priority->set_priority(0);
        appResourcesRequest->set_allocated_priority(priority);

        hadoop::yarn::ResourceProto *appResources = new hadoop::yarn::ResourceProto;
        appResources->set_memory(memory);
        appResources->set_virtual_cores(vcores);
        appResourcesRequest->set_allocated_capability(appResources);
        appResourcesRequest->set_num_containers(1);
        appResourcesRequest->set_relax_locality(true);

        hadoop::yarn::AllocateResponseProto response;

        this->service->allocate(this->controller, context, &response, NULL);

        cerr << "Printing allocate response:" << endl;
        printProtobufMessage(&response);

        // Check if allocation completed
        if (response.completed_container_statuses_size() > 0) {
            break;
        }

        for (auto container : response.allocated_containers()) {
            // Find associated token
            hadoop::yarn::NMTokenProto associated;
            for (auto token : response.nm_tokens()) {
                if (token.nodeid().host().compare(container.nodeid().host()) == 0 && token.nodeid().port() == container.nodeid().port()) {
                    associated = token;
                }
            }

            ContainerManager* manager = new ContainerManager(container.nodeid().host(), container.nodeid().port(), associated.token(), container.container_token(), this->effectiveUser, this->verbosity);

            // Generate command
            manager->startContainer(command);

            this->managers.push_back(manager);
        }

        if (!completed || !response.has_response_id()) {
            // Put delay
            sleep(2);
        }
    }
}

void ApplicationMaster::finishApplicationMaster(hadoop::yarn::FinalApplicationStatusProto finalStatus) {
    // Kill containers
    for (auto m : this->managers) {
        m->stopContainer();
        delete m;
    }

    hadoop::yarn::FinishApplicationMasterRequestProto* request = new hadoop::yarn::FinishApplicationMasterRequestProto;
    request->set_final_application_status(finalStatus);

    hadoop::yarn::FinishApplicationMasterResponseProto response;

    this->service->finishApplicationMaster(this->controller, request, &response, NULL);
}

void ApplicationMaster::putError(string err) {
    this->hasError = true;
    this->error = err;
}

void ApplicationMaster::clearError() {
    this->hasError = false;
    this->error = "";
}

void ApplicationMaster::parseTokenFile() {
    const char* tokenFile = getenv("HADOOP_TOKEN_FILE_LOCATION");

    if (tokenFile == NULL) {
        this->putError("Cannot find HADOOP_TOKEN_FILE_LOCATION env variable !");
        return;
    }

    creds.readTokenStorageFile(tokenFile);

    hadoop::common::TokenProto token = creds.getToken("");
    hadoop::yarn::AMRMTokenIdentifierProto amrmToken;
    amrmToken.ParseFromString(token.identifier());

    this->username = base64_encode(token.identifier());
    this->password = base64_encode(token.password());

    hadoop::yarn::ApplicationAttemptIdProto attemptID = amrmToken.appattemptid();
    hadoop::yarn::ApplicationIdProto appId = attemptID.application_id();
    stringstream newClientIDStream;
    newClientIDStream << "appattempt_" << appId.cluster_timestamp() << "_" << setfill('0') << setw(4) << appId.id() << "_" << setfill('0') << setw(6) << attemptID.attemptid();
    this->effectiveUser = newClientIDStream.str();

    cerr << "Token parsed: u(" << this->username << ") p(" << this->password << ")" << endl; 
}

}