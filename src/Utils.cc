#include <Yarn++/Utils.hpp>

#include <sys/ioctl.h>
#include <iostream>
#include <unistd.h>
#include <typeinfo>
#include <sstream>
#include <iomanip>
#include <vector>

#include <Yarn++/genprotos/yarn_security_tokens.pb.h>
#include <Yarn++/genprotos/yarn_service_protos.pb.h>
#include <Yarn++/genprotos/yarn_protos.pb.h>

using namespace std;

namespace Yarnpp
{

#define PROTOBUF_PREDICT_TRUE(x) (__builtin_expect(!!(x), 1))

    void FormatBytes(const void *bytes, size_t len)
    {
        char *bytesChar = (char *)bytes;
        size_t nbLines = len / 16;
        unsigned long address = 0;
        size_t i = 0;

        cerr << hex << setfill('0');
        for (size_t line = 0; line < nbLines + 1; ++line) {
            size_t remaining = line < nbLines ? 16 : len % 16;

            // Print the address
            cerr << setw(8) << address;

            // Print hex
            for (int i = 0; i < 16; ++i) {
                if (i % 8 == 0) {
                    cerr << ' ';
                }
                if (i < remaining) {
                    cerr << ' ' << setw(2) << (unsigned int)(unsigned char)bytesChar[line * 16 + i];
                } else {
                    cerr << "   ";
                }
            }

            // Print ascii
            cerr << "  ";
            for (int i = 0; i < remaining; ++i) {
                if (bytesChar[line * 16 + i] < 32) {
                    cerr << '.';
                } else {
                    cerr << bytesChar[line * 16 + i];
                }
            }

            cerr << "\n";
            address += 16;
        }

        cerr << dec;
    }

    std::pair<const char *, google::protobuf::uint32> VarintParseSlow32(const char *p, google::protobuf::uint32 res)
    {
        for (std::uint32_t i = 2; i < 5; i++)
        {
            google::protobuf::uint32 byte = static_cast<google::protobuf::uint8>(p[i]);
            res += (byte - 1) << (7 * i);
            if (PROTOBUF_PREDICT_TRUE(byte < 128))
            {
                return {p + i + 1, res};
            }
        }
        // Accept >5 bytes
        for (std::uint32_t i = 5; i < 10; i++)
        {
            google::protobuf::uint32 byte = static_cast<google::protobuf::uint8>(p[i]);
            if (PROTOBUF_PREDICT_TRUE(byte < 128))
            {
                return {p + i + 1, res};
            }
        }
        return {nullptr, 0};
    }

    const char *VarintParseSlow(const char *p, google::protobuf::uint32 res, google::protobuf::uint32 *out)
    {
        auto tmp = VarintParseSlow32(p, res);
        *out = tmp.second;
        return tmp.first;
    }

    const char *VarintParse(const char *p, google::protobuf::uint32 *out)
    {
        auto ptr = reinterpret_cast<const google::protobuf::uint8 *>(p);
        google::protobuf::uint32 res = ptr[0];
        if (!(res & 0x80))
        {
            *out = res;
            return p + 1;
        }
        google::protobuf::uint32 byte = ptr[1];
        res += (byte - 1) << 7;
        if (!(byte & 0x80))
        {
            *out = res;
            return p + 2;
        }
        return VarintParseSlow(p, res, out);
    }

    google::protobuf::uint32 ReadVarint32(const char **p)
    {
        google::protobuf::uint32 tmp;
        *p = VarintParse(*p, &tmp);
        return tmp;
    }

    typedef unsigned char ubyte;
    const auto BASE64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

    std::vector<ubyte> encode(const std::vector<ubyte> &source)
    {
        auto it = source.cbegin();
        auto end = source.cend();

        std::vector<ubyte> sink;
        while (it != end)
        {
            auto b1 = *it++;
            int acc;

            sink.push_back(BASE64[b1 >> 2]); // first output (first six bits from b1)

            acc = (b1 & 0x3) << 4; // last two bits from b1
            if (it != end)
            {
                auto b2 = *it++;
                acc |= (b2 >> 4); // first four bits from b2

                sink.push_back(BASE64[acc]); // second output

                acc = (b2 & 0xF) << 2; // last four bits from b2
                if (it != end)
                {
                    auto b3 = *it++;
                    acc |= (b3 >> 6); // first two bits from b3

                    sink.push_back(BASE64[acc]);       // third output
                    sink.push_back(BASE64[b3 & 0x3F]); // fouth output (final six bits from b3)
                }
                else
                {
                    sink.push_back(BASE64[acc]); // third output
                    sink.push_back('=');         // fourth output (1 byte padding)
                }
            }
            else
            {
                sink.push_back(BASE64[acc]); // second output
                sink.push_back('=');         // third output (first padding byte)
                sink.push_back('=');         // fourth output (second padding byte)
            }
        }
        return sink;
    }

    int findIndex(ubyte val)
    {
        if ('A' <= val && val <= 'Z')
        {
            return val - 'A';
        }
        if ('a' <= val && val <= 'z')
        {
            return val - 'a' + 26;
        }
        if ('0' <= val && val <= '9')
        {
            return val - '0' + 52;
        }
        if ('+' == val)
        {
            return 62;
        }
        if ('/' == val)
        {
            return 63;
        }
        return -1;
    }

    std::vector<ubyte> decode(const std::vector<ubyte> &source)
    {
        if (source.size() % 4 != 0)
        {
            throw new std::runtime_error("Error in size to the decode method");
        }

        auto it = source.cbegin();
        auto end = source.cend();

        std::vector<ubyte> sink;
        while (it != end)
        {
            auto b1 = *it++;
            auto b2 = *it++;
            auto b3 = *it++; // might be first padding byte
            auto b4 = *it++; // might be first or second padding byte

            auto i1 = findIndex(b1);
            auto i2 = findIndex(b2);
            auto acc = i1 << 2; // six bits came from the first byte
            acc |= i2 >> 4;     // two bits came from the first byte

            sink.push_back(acc); // output the first byte

            if (b3 != '=')
            {
                auto i3 = findIndex(b3);

                acc = (i2 & 0xF) << 4; // four bits came from the second byte
                acc |= i3 >> 2;        // four bits came from the second byte

                sink.push_back(acc); // output the second byte

                if (b4 != '=')
                {
                    auto i4 = findIndex(b4);

                    acc = (i3 & 0x3) << 6; // two bits came from the third byte
                    acc |= i4;             // six bits came from the third byte

                    sink.push_back(acc); // output the third byte
                }
            }
        }
        return sink;
    }

    std::string base64_encode(const std::string &in)
    {
        std::vector<ubyte> str{begin(in), end(in)};
        std::vector<ubyte> encoded = encode(str);

        std::string result(encoded.begin(), encoded.end());
        result.append("\x00");

        return result;
    }

    std::string base64_decode(const std::string &in)
    {
        std::vector<ubyte> str{begin(in), end(in)};
        std::vector<ubyte> decoded = decode(str);

        std::string result(decoded.begin(), decoded.end());
        result.append("\x00");

        return result;
    }

    /** PROTOBUF MESSAGE PRINTERS **/
    void printContainerTokenIdentifierProto(const hadoop::yarn::ContainerTokenIdentifierProto* proto, string tab) {
        cerr << tab << "=== ContainerTokenIdentifierProto ===" << endl;
        if (proto->has_containerid()) {
            cerr << tab << "+ Container id:" << endl;
            printProtobufMessage(&proto->containerid(), tab + "  ");
        } else {
            cerr << tab << "- No containerid" << endl;
        }
        if (proto->has_nmhostaddr()) {
            cerr << tab << "+ Node manager host addr: " << proto->nmhostaddr() << endl;
        } else {
            cerr << tab << "- No nmhostaddr" << endl;
        }
        if (proto->has_appsubmitter()) {
            cerr << tab << "+ App submitter: " << proto->appsubmitter() << endl;
        } else {
            cerr << tab << "- No appsubmitter" << endl;
        }
        if (proto->has_resource()) {
            cerr << tab << "+ Resource: " << endl;
            printProtobufMessage(&proto->resource(), tab + "  ");
        } else {
            cerr << tab << "- No resource" << endl;
        }
        if (proto->has_expirytimestamp()) {
            cerr << tab << "+ Expiry timestamp: " << proto->expirytimestamp() << endl;
        } else {
            cerr << tab << "- No expirytimestamp" << endl;
        }
        if (proto->has_masterkeyid()) {
            cerr << tab << "+ Master key id: " << proto->masterkeyid() << endl;
        } else {
            cerr << tab << "- No masterkeyid" << endl;
        }
        if (proto->has_rmidentifier()) {
            cerr << tab << "+ Resource manager identifier: " << proto->rmidentifier() << endl;
        } else {
            cerr << tab << "- No rmidentifier" << endl;
        }
        if (proto->has_priority()) {
            cerr << tab << "+ Priority: " << endl;
            printProtobufMessage(&proto->priority(), tab + "  ");
        } else {
            cerr << tab << "- No priority" << endl;
        }
        if (proto->has_creationtime()) {
            cerr << tab << "+ Creation time: " << proto->creationtime() << endl;
        } else {
            cerr << tab << "- No creationtime" << endl;
        }
        if (proto->has_logaggregationcontext()) {
            cerr << tab << "+ Log aggregation context: " << endl;
            printProtobufMessage(&proto->logaggregationcontext(), tab + "  ");
        } else {
            cerr << tab << "- No logaggregationcontext" << endl;
        }
        if (proto->has_nodelabelexpression()) {
            cerr << tab << "+ Node label expression: " << proto->nodelabelexpression() << endl;
        } else {
            cerr << tab << "- No nodelabelexpression" << endl;
        }
        if (proto->has_containertype()) {
            cerr << tab << "+ Container type:" << hadoop::yarn::ContainerTypeProto_Name(proto->containertype()) << endl;
        } else {
            cerr << tab << "- No containertype" << endl;
        }
        if (proto->has_executiontype()) {
            cerr << tab << "+ Execution type: " << hadoop::yarn::ExecutionTypeProto_Name(proto->executiontype()) << endl;
        } else {
            cerr << tab << "- No executiontype" << endl;
        }
        if (proto->has_version()) {
            cerr << tab << "+ Version: " << proto->version() << endl;
        } else {
            cerr << tab << "- No version" << endl;
        }
        if (proto->has_allocation_request_id()) {
            cerr << tab << "+ Allocation request id: " << proto->allocation_request_id() << endl;
        } else {
            cerr << tab << "- No allocation_request_id" << endl;
        }
        if (proto->allocation_tags_size() == 0) {
            cerr << tab << "- No allocation tag" << endl;
        } else {
            cerr << tab << "+ Allocation tags: " << endl;
            for (auto tag : proto->allocation_tags()) {
                cerr << tab << "  + " << tag << endl;
            }
        }
        cerr << tab << "=====================================" << endl;
    }

    void printTokenProto(const hadoop::common::TokenProto* proto, string tab) {
        cerr << tab << "=== TokenProto ===" << endl;
        cerr << tab << "+ Identifier:" << endl;
        FormatBytes(proto->identifier().data(), proto->identifier().length());
        cerr << tab << "+ Password: " << endl;
        FormatBytes(proto->password().data(), proto->password().length());
        cerr << tab << "+ Kind: " << proto->kind() << endl;
        cerr << tab << "+ Service: " << proto->service() << endl;
        cerr << tab << "==================" << endl;
    }
    
    void printNMTokenIdentifierProto(const hadoop::yarn::NMTokenIdentifierProto* proto, string tab) {
        cerr << tab << "=== NMTokenIdentifierProto ===" << endl;
        if (proto->has_appattemptid()) {
            cerr << tab << "+ Application attempt id: " << endl;
            printProtobufMessage(&proto->appattemptid(), tab + "  ");
        } else {
            cerr << tab << "- No appAttemptId" << endl;
        }
        if (proto->has_nodeid()) {
            cerr << tab << "+ Node id: " << endl;
            printProtobufMessage(&proto->nodeid(), tab + "  ");
        } else {
            cerr << tab << "- No nodeId" << endl;
        }
        if (proto->has_appsubmitter()) {
            cerr << tab << "+ Application submitter: " << proto->appsubmitter() << endl;
        } else {
            cerr << tab << "- No appSubmitter" << endl;
        }
        if (proto->has_keyid()) {
            cerr << tab << "+ Key id: " << proto->keyid() << endl;
        } else {
            cerr << tab << "- No keyId" << endl;
        }
        cerr << tab << "==============================" << endl;
    }

    void printApplicationAttemptIdProto(const hadoop::yarn::ApplicationAttemptIdProto* proto, string tab) {
        cerr << tab << "=== ApplicationAttemptIdProto ===" << endl;
        if (proto->has_application_id()) {
            cerr << tab << "+ Application id: " << endl;
            printProtobufMessage(&proto->application_id(), tab + "  ");
        } else {
            cerr << tab << "- No applicationId" << endl;
        }
        if (proto->has_attemptid()) {
            cerr << tab << "+ Attempt id: " << proto->attemptid() << endl;
        } else {
            cerr << tab << "- No attemptId" << endl;
        }
        cerr << tab << "=================================" << endl;
    }

    void printApplicationIdProto(const hadoop::yarn::ApplicationIdProto* proto, string tab) {
        cerr << tab << "=== ApplicationIdProto ===" << endl;
        if (proto->has_id()) {
            cerr << tab << "+ Id: " << proto->id() << endl;
        } else {
            cerr << tab << "- No id" << endl;
        }
        if (proto->has_cluster_timestamp()) {
            cerr << tab << "+ Cluster timestamp: " << proto->cluster_timestamp() << endl;
        } else {
            cerr << tab << "- No cluster timestamp" << endl;
        }
        cerr << tab << "==========================" << endl;
    }

    void printNodeIdProto(const hadoop::yarn::NodeIdProto* proto, string tab) {
        cerr << tab << "=== NodeIdProto ===" << endl;
        if (proto->has_host()) {
            cerr << tab << "+ Host: " << proto->host() << endl;
        } else {
            cerr << tab << "- No host" << endl;
        }
        if (proto->has_port()) {
            cerr << tab << "+ Port: " << proto->port() << endl;
        } else {
            cerr << tab << "- No port" << endl;
        }
        cerr << tab << "===================" << endl;
    }

    void printStartContainersResponseProto(const hadoop::yarn::StartContainersResponseProto* proto, string tab) {
        cerr << tab << "=== StartContainersResponseProto ===" << endl;
        if (proto->services_meta_data_size() == 0) {
            cerr << tab << "- No services_meta_data" << endl;
        } else {
            cerr << tab << "+ Service metadata:" << endl;
            for (auto item : proto->services_meta_data()) {
                cerr << tab << "  " << item.key() << ":" << endl;
                FormatBytes(item.value().data(), item.value().length());
            }
        }
        if (proto->succeeded_requests_size() == 0) {
            cerr << tab << "- No succeeded_requests" << endl;
        } else {
            cerr << tab << "+ Succeeded requests:" << endl;
            for (auto item : proto->succeeded_requests()) {
                printProtobufMessage(&item, tab + "  ");
            }
        }
        if (proto->failed_requests_size() == 0) {
            cerr << tab << "- No failed_requests" << endl;
        } else {
            cerr << tab << "+ Failed requests:" << endl;
            for (auto item : proto->failed_requests()) {
                printProtobufMessage(&item, tab + "  ");
            }
        }
        cerr << tab << "====================================" << endl;
    }

    void printAllocateResponseProto(const hadoop::yarn::AllocateResponseProto* proto, string tab) {
        cerr << tab << "=== AllocateResponseProto ===" << endl;
        if (proto->has_a_m_command()) {
            cerr << tab << "+ Application master command: " << hadoop::yarn::AMCommandProto_Name(proto->a_m_command()) << endl;
        } else {
            cerr << tab << "- No a_m_command" << endl;
        }
        if (proto->has_response_id()) {
            cerr << tab << "+ Response id: " << proto->response_id() << endl;
        } else {
            cerr << tab << "- No response_id" << endl;
        }
        if (proto->allocated_containers_size() == 0) {
            cerr << tab << "- No allocated_containers" << endl;
        } else {
            cerr << tab << "+ Allocated containers:" << endl;
            for (auto item : proto->allocated_containers()) {
                printProtobufMessage(&item, tab + "  ");
            }
        }
        if (proto->completed_container_statuses_size() == 0) {
            cerr << tab << "- No completed_container_statuses" << endl;
        } else {
            cerr << tab << "+ Completed container statuses:" << endl;
            for (auto item : proto->completed_container_statuses()) {
                printProtobufMessage(&item, tab + "  ");
            }
        }
        if (proto->has_limit()) {
            cerr << tab << "+ Limit:" << endl;
            printProtobufMessage(&proto->limit(), tab + "  ");
        } else {
            cerr << tab << "- No limit" << endl;
        }
        if (proto->updated_nodes_size() == 0) {
            cerr << tab << "- No updated nodes" << endl;
        } else {
            cerr << tab << "+ Updated nodes:" << endl;
            for (auto item : proto->updated_nodes()) {
                printProtobufMessage(&item, tab + "  ");
            }
        }
        if (proto->has_num_cluster_nodes()) {
            cerr << tab << "+ Number of cluster nodes: " << proto->num_cluster_nodes() << endl;
        } else {
            cerr << tab << "- No num_cluster_nodes" << endl;
        }
        if (proto->has_preempt()) {
            cerr << tab << "+ Preemption message:" << endl;
            printProtobufMessage(&proto->preempt(), tab + "  ");
        } else {
            cerr << tab << "- No preempt" << endl;
        }
        if (proto->nm_tokens_size() == 0) {
            cerr << tab << "- No node manager tokens" << endl;
        } else {
            cerr << tab << "+ Node manager tokens:" << endl;
            for (auto item : proto->nm_tokens()) {
                printProtobufMessage(&item, tab + "  ");
            }
        }
        if (proto->has_am_rm_token()) {
            cerr << tab << "+ Application<->Resource manager token:" << endl;
            printProtobufMessage(&proto->am_rm_token(), tab + "  ");
        } else {
            cerr << tab << "- No am_rm_token" << endl;
        }
        if (proto->has_application_priority()) {
            cerr << tab << "+ Application priority:" << endl;
            printProtobufMessage(&proto->application_priority(), tab + "  ");
        } else {
            cerr << tab << "- No application_priority" << endl;
        }
        if (proto->has_collector_info()) {
            cerr << tab << "+ Collector info:" << endl;
            printProtobufMessage(&proto->collector_info(), tab + "  ");
        } else {
            cerr << tab << "- No collector_info" << endl;
        }
        if (proto->update_errors_size() == 0) {
            cerr << tab << "- No update_errors" << endl;
        } else {
            cerr << tab << "+ Update errors:" << endl;
            for (auto item : proto->update_errors()) {
                printProtobufMessage(&item, tab + "  ");
            }
        }
        if (proto->updated_containers_size() == 0) {
            cerr << tab << "- No updated_containers" << endl;
        } else {
            cerr << tab << "+ Udpdated containers:" << endl;
            for (auto item : proto->updated_containers()) {
                printProtobufMessage(&item, tab + "  ");
            }
        }
        if (proto->containers_from_previous_attempts_size() == 0) {
            cerr << tab << "- No containers_from_previous_attempts" << endl;
        } else {
            cerr << tab << "+ Containers from previous attempts:" << endl;
            for (auto item : proto->containers_from_previous_attempts()) {
                printProtobufMessage(&item, tab + "  ");
            }
        }
        if (proto->rejected_scheduling_requests_size() == 0) {
            cerr << tab << "- No rejected_scheduling_requests" << endl;
        } else {
            cerr << tab << "+ Rejected scheduling requests:" << endl;
            for (auto item : proto->rejected_scheduling_requests()) {
                printProtobufMessage(&item, tab + "  ");
            }
        }
        cerr << tab << "=============================" << endl;
    }

    void printContainerStatusProto(const hadoop::yarn::ContainerStatusProto* proto, string tab) {
        cerr << tab << "=== ContainerStatusProto ===" << endl;
        if (proto->has_container_id()) {
            cerr << tab << "+ Container id:" << endl;
            printProtobufMessage(&proto->container_id(), tab + "  ");
        } else {
            cerr << tab << "- No container_id" << endl;
        }
        if (proto->has_state()) {
            cerr << tab << "+ State: " << hadoop::yarn::ContainerStateProto_Name(proto->state()) << endl;
        } else {
            cerr << tab << "- No state" << endl;
        }
        if (proto->has_diagnostics()) {
            cerr << tab << "+ Diagnostics: " << proto->diagnostics() << endl;
        } else {
            cerr << tab << "- No diagnostics" << endl;
        }
        if (proto->has_exit_status()) {
            cerr << tab << "+ Exit status: " << proto->exit_status() << endl;
        } else {
            cerr << tab << "- No exit_status" << endl;
        }
        if (proto->has_capability()) {
            cerr << tab << "+ Capability: " << endl;
            printProtobufMessage(&proto->capability(), tab + "  ");
        } else {
            cerr << tab << "- No capability" << endl;
        }
        if (proto->has_executiontype()) {
            cerr << tab << "+ Execution type: " << hadoop::yarn::ExecutionTypeProto_Name(proto->executiontype()) << endl;
        } else {
            cerr << tab << "- No executionType" << endl;
        }
        if (proto->container_attributes_size() == 0){
            cerr << tab << "- No container attributes" << endl;
        } else {
            cerr << tab << "+ Container attributes:" << endl;
            for (auto item : proto->container_attributes()) {
                cerr << tab << "  + " << item.key() << ": " << item.value() << endl;
            }
        }
        if (proto->has_container_sub_state()) {
            cerr << tab << "+ Container sub state: " << hadoop::yarn::ContainerSubStateProto_Name(proto->container_sub_state()) << endl;
        } else {
            cerr << tab << "- No container sub state" << endl;
        }
        cerr << tab << "============================" << endl;
    }

    void printContainerIdProto(const hadoop::yarn::ContainerIdProto* proto, string tab) {
        cerr << tab << "=== ContainerIdProto ===" << endl;
        if (proto->has_app_id()) {
            cerr << tab << "+ Application id: " << endl;
            printProtobufMessage(&proto->app_id(), tab + "  ");
        } else {
            cerr << tab << "- No application id" << endl;
        }
        if (proto->has_app_attempt_id()) {
            cerr << tab << "+ Application attempt id: " << endl;
            printProtobufMessage(&proto->app_attempt_id(), tab + "  ");
        } else {
            cerr << tab << "- No application attempt id" << endl;
        }
        if (proto->has_id()) {
            cerr << tab << "+ Application id: " << proto->id() << endl;
        } else {
            cerr << tab << "- No application id" << endl;
        }
        cerr << tab << "========================" << endl;
    }

    void printResourceProto(const hadoop::yarn::ResourceProto* proto, string tab) {
        cerr << tab << "=== ResourceProto ===" << endl;
        if (proto->has_memory()) {
            cerr << tab << "+ Memory: " << proto->memory() << endl;
        } else {
            cerr << tab << "- No memory" << endl;
        }
        if (proto->has_virtual_cores()) {
            cerr << tab << "+ Virtual cores: " << proto->virtual_cores() << endl;
        } else {
            cerr << tab << "- No virtual cores" << endl;
        }
        if (proto->resource_value_map_size() == 0){
            cerr << tab << "- No resource value map" << endl;
        } else {
            cerr << tab << "+ Resource value map:" << endl;
            for (auto item : proto->resource_value_map()) {
                cerr << tab << "  + " << item.key() << ": " << item.value() << item.units() << endl;
            }
        }
        cerr << tab << "=====================" << endl;
    }

    void printPriorityProto(const hadoop::yarn::PriorityProto* proto, string tab) {
        cerr << tab << "=== PriorityProto ===" << endl;
        if (proto->has_priority()) {
            cerr << tab << "+ Priotity: " << proto->priority() << endl;
        } else {
            cerr << tab << "- No priority" << endl;
        }
        cerr << tab << "=====================" << endl;
    }

    void printContainerProto(const hadoop::yarn::ContainerProto* proto, string tab) {
        cerr << tab << "=== ContainerProto ===" << endl;
        if (proto->has_id()) {
            cerr << tab << "+ Container id: " << endl;
            printProtobufMessage(&proto->id(), tab + "  ");
        } else {
            cerr << tab << "- No id" << endl;
        }
        if (proto->has_nodeid()) {
            cerr << tab << "+ Node id: " << endl;
            printProtobufMessage(&proto->nodeid(), tab + "  ");
        } else {
            cerr << tab << "- No nodeId" << endl;
        }
        if (proto->has_node_http_address()) {
            cerr << tab << "+ Node http address: " << proto->node_http_address() << endl;
        } else {
            cerr << tab << "- No node_http_address" << endl;
        }
        if (proto->has_resource()) {
            cerr << tab << "+ Resource:" << endl;
            printProtobufMessage(&proto->resource(), tab + "  ");
        } else {
            cerr << tab << "- No resource" << endl;
        }
        if (proto->has_priority()) {
            cerr << tab << "+ Priority: " << endl;
            printProtobufMessage(&proto->priority(), tab + "  ");
        } else {
            cerr << tab << "- No priority" << endl;
        }
        if (proto->has_container_token()) {
            cerr << tab << "+ Container token: " << endl;
            printProtobufMessage(&proto->container_token(), tab + "  ");
        } else {
            cerr << tab << "- No container_token" << endl;
        }
        if (proto->has_execution_type()) {
            cerr << tab << "+ Execution type: " << hadoop::yarn::ExecutionTypeProto_Name(proto->execution_type()) << endl;
        } else {
            cerr << tab << "- No execution_type" << endl;
        }
        if (proto->has_allocation_request_id()) {
            cerr << tab << "+ Allocation request id: " << proto->allocation_request_id() << endl;
        } else {
            cerr << tab << "- No allocation_request_id" << endl;
        }
        if (proto->has_version()) {
            cerr << tab << "+ Version: " << proto->version() << endl;
        } else {
            cerr << tab << "- No version" << endl;
        }
        if (proto->allocation_tags_size() == 0) {
            cerr << tab << "- No allocation tags" << endl;
        } else {
            cerr << tab << "+ Allocation tags: " << endl;
            for (auto item : proto->allocation_tags()) {
                cerr << tab << "  + " << item << endl;
            }
        }
        cerr << tab << "======================" << endl;
    }

    void printNMTokenProto(const hadoop::yarn::NMTokenProto* proto, string tab) {
        cerr << tab << "=== NMTokenProto ===" << endl;
        if (proto->has_nodeid()) {
            cerr << tab << "+ Node id:" << endl;
            printProtobufMessage(&proto->nodeid(), tab + "  ");
        } else {
            cerr << tab << "- No node id" << endl;
        }
        if (proto->has_token()) {
            cerr << tab << "+ Token: " << endl;
            printProtobufMessage(&proto->token(), tab + "  ");
        } else {
            cerr << tab << "- No token" << endl;
        }
        cerr << tab << "====================" << endl;
    }

    void printProtobufMessage(const ::google::protobuf::Message* msg, string tab) {
        if (typeid(*msg) == typeid(hadoop::yarn::ContainerTokenIdentifierProto)) {
            printContainerTokenIdentifierProto(dynamic_cast<const hadoop::yarn::ContainerTokenIdentifierProto*>(msg), tab);
        } else if (typeid(*msg) == typeid(hadoop::common::TokenProto)) {
            printTokenProto(dynamic_cast<const hadoop::common::TokenProto*>(msg), tab);
        } else if (typeid(*msg) == typeid(hadoop::yarn::NMTokenIdentifierProto)) {
            printNMTokenIdentifierProto(dynamic_cast<const hadoop::yarn::NMTokenIdentifierProto*>(msg), tab);
        } else if (typeid(*msg) == typeid(hadoop::yarn::ApplicationAttemptIdProto)) {
            printApplicationAttemptIdProto(dynamic_cast<const hadoop::yarn::ApplicationAttemptIdProto*>(msg), tab);
        } else if (typeid(*msg) == typeid(hadoop::yarn::ApplicationIdProto)) {
            printApplicationIdProto(dynamic_cast<const hadoop::yarn::ApplicationIdProto*>(msg), tab);
        } else if (typeid(*msg) == typeid(hadoop::yarn::NodeIdProto)) {
            printNodeIdProto(dynamic_cast<const hadoop::yarn::NodeIdProto*>(msg), tab);
        } else if (typeid(*msg) == typeid(hadoop::yarn::StartContainersResponseProto)) {
            printStartContainersResponseProto(dynamic_cast<const hadoop::yarn::StartContainersResponseProto*>(msg), tab);
        } else if (typeid(*msg) == typeid(hadoop::yarn::AllocateResponseProto)) {
            printAllocateResponseProto(dynamic_cast<const hadoop::yarn::AllocateResponseProto*>(msg), tab);
        } else if (typeid(*msg) == typeid(hadoop::yarn::ContainerStatusProto)) {
            printContainerStatusProto(dynamic_cast<const hadoop::yarn::ContainerStatusProto*>(msg), tab);
        } else if (typeid(*msg) == typeid(hadoop::yarn::ContainerIdProto)) {
            printContainerIdProto(dynamic_cast<const hadoop::yarn::ContainerIdProto*>(msg), tab);
        } else if (typeid(*msg) == typeid(hadoop::yarn::ResourceProto)) {
            printResourceProto(dynamic_cast<const hadoop::yarn::ResourceProto*>(msg), tab);
        } else if (typeid(*msg) == typeid(hadoop::yarn::PriorityProto)) {
            printPriorityProto(dynamic_cast<const hadoop::yarn::PriorityProto*>(msg), tab);
        } else if (typeid(*msg) == typeid(hadoop::yarn::ContainerProto)) {
            printContainerProto(dynamic_cast<const hadoop::yarn::ContainerProto*>(msg), tab);
        } else if (typeid(*msg) == typeid(hadoop::yarn::NMTokenProto)) {
            printNMTokenProto(dynamic_cast<const hadoop::yarn::NMTokenProto*>(msg), tab);
        } else {
            cerr << tab << "=== PROTOBUF MESSAGE NOT KNOWN ===" << endl;
        }
    }
}