#include <Yarn++/Verbosity.hpp>
#include <iostream>

using namespace std;

namespace Yarnpp
{
    namespace Verbose {

        void listVerbosityCodes() {
            cout << "Verbosity codes:" << endl;
            cout << "Packets:   " << Packets << endl;
            cout << "Resources: " << Resources << endl;
        }

    }
}