#include <iostream>
#include <getopt.h>
#include <cstdlib>
#include <string>

#include <Yarn++/ApplicationMaster.hpp>

void usage() {
    cout << "appMaster -h hostname -p port [-v verbosity]" << endl;
    cout << "  - hostname: Hostname of Yarn Resource Manager" << endl;
    cout << "  - port:     Port of Yarn Resource Manager" << endl;
    cout << "  - v:        Verbosity of application master" << endl;
}

int main(int argc, char* argv[]) {
    // Parsing arguments for host and port
    int port = -1;
    int verbosity = 0;
    string host = "";
    bool exit = false;

    while (!exit) {
        switch(getopt(argc, argv, "h:p:v:")) {
            case '?':
                usage();
                break;
            case 'h':
                host.append(optarg);
                break;
            case 'p':
                port = atoi(optarg);
                break;
            case 'v':
                verbosity = atoi(optarg);
                break;
            case -1:
                exit = true;
                break;
        }
    }

    if (port == -1 || host.length() == 0) {
        cerr << "Missing arguments." << endl;
        usage();
        return 1;
    }

    Yarnpp::ApplicationMaster master(host, port, verbosity);

    master.registerApplicationMaster();

    master.finishApplicationMaster(Yarnpp::ApplicationStatus::APP_SUCCEEDED);

    return EXIT_SUCCESS;
}
