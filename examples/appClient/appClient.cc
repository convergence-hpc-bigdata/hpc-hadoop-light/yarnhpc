#include <iostream>
#include <unistd.h>
#include <getopt.h>
#include <sstream>
#include <string>
#include <vector>

#include <Yarn++/ApplicationClient.hpp>
#include <Yarn++/Verbosity.hpp>

using namespace std;

void usage() {
    cout << "appClient -h hostname -p port -m master -a masterport [-e] [-l] [-v verbosity]" << endl;
    cout << "  -h hostname:   Hostname of Yarn Resource Manager" << endl;
    cout << "  -p port:       Port of Yarn Resource Manager" << endl;
    cout << "  -m master:     Application master file path" << endl;
    cout << "  -a masterport: Port of Yarn Resource Manager for application master" << endl;
    cout << "  -e:            Copy environment variables in application master process" << endl;
    cout << "  -l:            List verbosity codes" << endl;
    cout << "  -v verbosity:  Set verbosity" << endl;
}

int main(int argc, char* argv[], char* envp[]) {
    // Parsing arguments for host and port
    int port = -1;
    int masterport = -1;
    int verbosity = 0;
    string host = "";
    string master = "";
    bool exit = false;
    bool copyEnv = false;

    while (!exit) {
        switch(getopt(argc, argv, "h:p:m:a:v:le")) {
            case '?':
                usage();
                break;
            case 'h':
                host.append(optarg);
                break;
            case 'p':
                port = atoi(optarg);
                break;
            case 'm':
                master.append(optarg);
                break;
            case 'a':
                masterport = atoi(optarg);
                break;
            case 'e':
                copyEnv = true;
                break;
            case 'l':
                Yarnpp::Verbose::listVerbosityCodes();
                return EXIT_SUCCESS;
                break;
            case 'v':
                verbosity = atoi(optarg);
                break;
            case -1:
                exit = true;
                break;
        }
    }

    if (port == -1 || host.length() == 0 || master.length() == 0 || masterport == -1) {
        cerr << "Missing arguments." << endl;
        usage();
        return 1;
    }

    Yarnpp::ApplicationClient client(host, port, "appClient", verbosity);

    cout << "Creating application..." << endl;
    int32_t appId = client.createApplication();
    cout << "Got application ID: " << appId << endl;

    Yarnpp::Resources resources = client.getAssociatedCapabilities(appId);
    cout << "Associated capabilities:" << endl;
    cout << "Memory: " << resources.memory() << endl;
    cout << "Virtual cores: " << resources.virtual_cores() << endl;
    cout << "Resources:" << endl;
    for (auto itr = resources.resource_value_map().begin(); itr != resources.resource_value_map().end(); ++itr) {
        cout << "  - " << (*itr).key() << ": " << (*itr).value() << " " << (*itr).units() << endl;
    }

    // Creating command from options
    stringstream commandStream;
    commandStream << master << " -h " << host << " -p " << masterport << " -v " << verbosity;
    string command = commandStream.str();

    // Setting environment variables
    vector<pair<string, string>> envVars;
    if (copyEnv) {
        for (char** env = envp; *env != 0; ++env) {
            string token = string(*env);
            size_t pos = token.find('=');
            envVars.push_back({token.substr(0, pos), token.substr(pos + 1)});
        }
    }

    cout << "Submitting app with command: " << command << endl;
    client.submitApplication(appId, "appClient example", 1024, 1, command, envVars);

    Yarnpp::ApplicationReport report = client.getReport(appId);

    while (report.final_application_status() == hadoop::yarn::FinalApplicationStatusProto::APP_UNDEFINED) {
        sleep(2);
        report = client.getReport(appId);
    }

    cout << "Printing report" << endl;
    client.printReport(appId);

    cout << "Killing application..." << endl;
    client.deleteApplication(appId);
    cout << "End" << endl;

    return EXIT_SUCCESS;
}
