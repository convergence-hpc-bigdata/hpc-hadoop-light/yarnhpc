# Target
TARGET = libyarn++

# Binaries
CXX    = g++
PROTOC = protoc

# Directories
SRC_DIR        = src
PROTOS_DIR     = protobuf
PROTOS_GEN_DIR = genprotos
LOG_DIR        = .logs
OUTPUT_DIR     = lib

INSTALL_INCLUDE = /usr/local/include/
INSTALL_LIBS    = /usr/local/lib/$(TARGET).so

# Sources target
SRCS = $(shell find ${SRC_DIR} -name '*.cc')
OBJS = $(SRCS:%.cc=%.o)
DEPS = $(OBJS:%.o=%.d)

# Protobuf generation
PROTOS      = $(notdir $(shell find $(PROTOS_DIR) -name '*.proto'))
GENPROTOS   = $(addprefix $(PROTOS_GEN_DIR)/, $(PROTOS:%.proto=%.pb.cc))
BUILDPROTOS = $(GENPROTOS:%.pb.cc=%.pb.o)

# Library
OUTPUT = $(OUTPUT_DIR)/$(TARGET).so

# Examples
EXAMPLES = appClient appMaster

# Options
WITH_HWLOC := 0
WITH_HDFS := 0

# Compilation flags
CXXFLAGS += $$(pkg-config --cflags protobuf libsasl2) -std=c++11 -fPIC -Wall -Wextra -O2 -I include -I 3rd
LDFLAGS  += $$(pkg-config --libs protobuf libsasl2) -s -pthread -shared

# Check options
ifeq ($(WITH_HWLOC), 1)
CXXFLAGS += $$(pkg-config --cflags hwloc) -D WITH_HWLOC
LDFLAGS  += $$(pkg-config --libs hwloc)
endif
ifeq ($(WITH_HDFS), 1)
CXXFLAGS += -D WITH_HDFS
LDFLAGS  += -lhdfspp_static -ltools_common
endif