# define standard colors
ifneq (,$(findstring xterm,${TERM}))
	BLACK        := $(shell tput setaf 0)
	RED          := $(shell tput setaf 1)
	GREEN        := $(shell tput setaf 2)
	YELLOW       := $(shell tput setaf 3)
	LIGHTPURPLE  := $(shell tput setaf 4)
	PURPLE       := $(shell tput setaf 5)
	BLUE         := $(shell tput setaf 6)
	WHITE        := $(shell tput setaf 7)
	RESET 		 := $(shell tput sgr0)
	BOLD		 := $(shell tput bold)
else
	BLACK        := ""
	RED          := ""
	GREEN        := ""
	YELLOW       := ""
	LIGHTPURPLE  := ""
	PURPLE       := ""
	BLUE         := ""
	WHITE        := ""
	RESET        := ""
	BOLD		 := ""
endif

define call_and_test
	@if $(1) 2> $(LOG_DIR)/$(notdir $@).log; then \
		echo "${BOLD}$(2)[$(3)] $@${RESET}"; \
	else \
		echo "${BOLD}${RED}[$(3)] $@${RESET}"; \
	fi
endef