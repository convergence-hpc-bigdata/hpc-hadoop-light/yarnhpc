# Yarn++

## Introduction

> Yarn++ is a library used to launch C++ applications on a Hadoop v3.2.1* cluster developed in the context of the convergence between HPC and Big Data. It allows to create application client and application master to submit YARN applications and run C++ code on compute node.

<sup>*: With hadoop 3.3.0, protobuf is upgraded to 3.7.1 because protobuf 2.5.0 reached End Of Life.</sup>

## Build

libyarn++ requires some installed components:
1. Protobuf v2.5.0: https://github.com/protocolbuffers/protobuf/releases/tag/v2.5.0

After downloading this archive, you can then follow the regular steps for compiling the sources and installing protobuf:

```shell
$ sudo apt-get install autoconf automake libtool curl make g++ unzip
$ cd protobuf
$ git submodule update --init --recursive
$ ./autogen.sh
$ ./configure
$ make
$ make check
$ sudo make install
$ sudo ldconfig
```

2. libsasl2 v2.1.27: https://github.com/cyrusimap/cyrus-sasl/releases/tag/cyrus-sasl-2.1.27

Also here, after extracting the archive, you can follow these steps:

```shell
$ cd (directory you extracted to)
$ ./configure
$ make
$ make install
```

After installing these libraries, make sure they are available to libyarn++ during compilation by running:

```shell
$ make check
== Checking Protobuf ==
+ Protobuf cflags found
+ Protobuf libs found
+ protoc binary found
+ protoc has the right version: 2.5.0
== Protobuf OK ==
== Checking SASL ==
+ SASL cflags found
+ SASL libs found
== SASL OK ==
```

If the check is successfull, then you can compile the library with:

```shell
$ make lib
$ sudo make install
```

You can then compile the examples with:

```shell
$ make examples
```

### Options

Different options are available when compiling libyarn++:

1. WITH_HWLOC

2. WITH_HDFS

Before using hdfs api, you have to compile the native library from the hadoop sources. After downloading hadoop-3.2.2, follow this steps:

```shell
$ cd hadoop-hdfs-project/hadoop-hdfs-native-client/src/main/native/libhdfspp
$ cmake -DHDFSPP_LIBRARY_ONLY=ON
$ make install (with -j option if possible)
```

You can now use WITH_HDFS=1 when checking and compiling libyarn++.

## Documentation

### Compiling your application

To compile your own application using libyarn++, you need to link some libraries and include files. You can use:

```makefile
LDFLAGS += -L(path to libyarn++.so) -lyarn++
CXXFLAGS += -I(path to genprotos folder) -I(path to Yarn++ include folder)
```

Take note that when launching your new application you will need libyarn++ in your library path as well as the protobuf and sasl libraries.

### Using libyarn++

Your application is split in two parts: the application client and application master. The first one is the application that will get a new application ID from yarn and setup the environment to launch the application master in a node container. The latter is the application that will run in the hadoop cluster. The application master can also launch new container to distribute the work.

### Application client

To create an application client, you need to use the ApplicationClient class. The constructor takes up to 4 arguments:

```C++
Yarnpp::ApplicationClient(host, port, username, verbosity = 0)
```

The first two are the host and port of the Yarn application client server (port 8032 by default). The username is used to communicate with Yarn and the verbosity (disabled by default) will print evert transmission with hadoop.

This client allows you to communicate with Yarn to submit an application, you need to ask for an application id before subimitting it:

```C++
int32_t appID = client.createApplication();
client.submitApplication(appId, name, memory, vcores, appMasterCommand);
```

You can then get reports of your application with:

```C++
ApplicationReport appReport = client.getReport(appId);
```

Finally, you can force kill your application with:

```C++
client.deleteApplication(appId);
```

### Application master

Once your application client is ready, you can create an application master. It can be any C++ application but you have to create an ApplicationMaster (AM) object at the start of your program to register it as an AM with:

```C++
Yarnpp::ApplicationMaster master(host, port, verbosity);
master.registerApplicationMaster();
```

The first two arguments of the constructor are the host and port of the Yarn application master server (port 8030 by default). The registration is needed to notify Yarn that the application is running.

At the end of your program, you have to notify Yarn with the following function:

```C++
master.finishApplicationMaster(Yarnpp::ApplicationStatus::APP_SUCCEEDED);
```

It will update the Yarn application report with the final status and will delete all entries regarding your yarn application. You can find the final status in [include/Yarn++/ApplicationMaster.hpp](./include/Yarn++/ApplicationMaster.hpp):

```C++
APP_UNDEFINED
APP_SUCCEEDED
APP_FAILED
APP_KILLED
APP_ENDED
```

## Code samples

In the examples directory you can find two quick examples of an application client and an application master. You can compile these examples with make examples at the root of this project.
