include utils/vars.make
include utils/functions.make

.PHONY: clean check install

all: config
	@# Way to make a barrier if -j option is used, we need to generate all protobuf files before anything else !
	@${MAKE} -s --no-print-directory $(OUTPUT)

config:
	@# Generate build directories
	@mkdir -p "include/Yarn++/genprotos"
	@mkdir -p $(PROTOS_GEN_DIR)
	@mkdir -p $(LOG_DIR)
	@mkdir -p $(OUTPUT_DIR)
	@${MAKE} -s --no-print-directory $(GENPROTOS)

examples: all
	@for example in $(EXAMPLES); do \
		${MAKE} --no-print-directory -C examples/$$example; \
	done

$(OUTPUT): $(BUILDPROTOS) $(OBJS)
	$(call call_and_test, $(CXX) $^ $(LDFLAGS) -o $@,${BLUE},OUT)

-include $(DEPS)

%.o: %.cc
	$(call call_and_test, $(CXX) -MMD -c $< $(CXXFLAGS) -o $@,${GREEN},CXX)

$(PROTOS_GEN_DIR)/%.pb.cc:
	@# Recompute dep name because target and dep are not in same dir
	$(call call_and_test,$(PROTOC) -I $(PROTOS_DIR) --cpp_out=$(PROTOS_GEN_DIR) $(PROTOS_DIR)/$(notdir $(subst .pb.cc,.proto,$@)),${LIGHTPURPLE}, PB)
	@cp $(subst .cc,.h,$@) include/Yarn++/$(subst .cc,.h,$@)

check:
	@# Check if sasl and protobuf are accessible
	@echo "${BOLD}${BLUE}== Checking Protobuf ==${RESET}"
	@(pkg-config --cflags protobuf > /dev/null 2>&1 && echo "${BOLD}${GREEN}+ Protobuf cflags found${RESET}") || (echo "${BOLD}${RED}- Protobuf cflags not found${RESET}")
	@(pkg-config --libs protobuf > /dev/null 2>&1 && echo "${BOLD}${GREEN}+ Protobuf libs found${RESET}") || (echo "${BOLD}${RED}- Protobuf libs not found${RESET}")
	@(command -v $(PROTOC) > /dev/null 2>&1 && echo "${BOLD}${GREEN}+ ${PROTOC} binary found${RESET}") || (echo "${BOLD}${RED}- Cannot found ${PROTOC} binary in the path${RESET}")
	@if test "$$(${PROTOC} --version)" = "libprotoc 2.5.0"; then \
		echo "${BOLD}${GREEN}+ ${PROTOC} has the right version: 2.5.0${RESET}"; \
	else \
		echo "${BOLD}${RED}+ ${PROTOC} has not the right version: $$(${PROTOC} --version)${RESET}"; \
	fi
	@echo "${BOLD}${BLUE}== Protobuf OK ==${RED}"

	@echo "${BOLD}${BLUE}== Checking SASL ==${RESET}"
	@(pkg-config --cflags libsasl2 > /dev/null 2>&1 && echo "${BOLD}${GREEN}+ SASL cflags found${RESET}") || (echo "${BOLD}${RED}- SASL cflags not found${RESET}")
	@(pkg-config --libs libsasl2 > /dev/null 2>&1 && echo "${BOLD}${GREEN}+ SASL libs found${RESET}") || (echo "${BOLD}${RED}- SASL libs not found${RESET}")
	@echo "${BOLD}${BLUE}== SASL OK ==${RED}"

	@echo "${BOLD}${BLUE}== Checking options ==${RESET}"
ifeq (${WITH_HWLOC}, 1)
	@(pkg-config --cflags hwloc > /dev/null 2>&1 && echo "${BOLD}${GREEN}+ HWLOC cflags found${RESET}") || (echo "${BOLD}${RED}- HWLOC cflags not found${RESET}")
	@(pkg-config --libs hwloc > /dev/null 2>&1 && echo "${BOLD}${GREEN}+ HWLOC libs found${RESET}") || (echo "${BOLD}${RED}- HWLOC libs not found${RESET}")
endif
ifeq (${WITH_HDFS}, 1)
	@($(CXX) -lhdfspp_static 2>&1 | grep -q "lhdfspp_static" && echo "${BOLD}${RED}- HDFS library (hdfspp_static) not in library path...${RESET}") || (echo "${BOLD}${GREEN}+ hdfspp_static library found${RESET}")
	@($(CXX) -ltools_common 2>&1 | grep -q "ltools_common" && echo "${BOLD}${RED}- HDFS library (tools_common) not in library path...${RESET}") || (echo "${BOLD}${GREEN}+ tools_common library found${RESET}")
	@($(CXX) -std=c++11 utils/check_hdfs.cc 2>&1 | grep -q "hdfspp/hdfspp.h" && echo "${BOLD}${RED}- HDFS headers not found${RESET}") || (echo "${BOLD}${GREEN}+ HDFS headers found${RESET}")
	@($(CXX) -std=c++11 utils/check_hdfs.cc 2>&1 | grep -q "tools_common.h" && echo "${BOLD}${RED}- HDFS tools headers not found${RESET}") || (echo "${BOLD}${GREEN}+ HDFS tools headers found${RESET}")

endif
	@echo "${BOLD}${BLUE}== Options OK ==${RESET}"

clean:
	@rm -rf $(OBJS) $(DEPS) $(PROTOS_GEN_DIR) $(LOG_DIR) $(OUTPUT) include/Yarn++/genprotos/*
	@${MAKE} --no-print-directory -C examples/appClient clean
	@${MAKE} --no-print-directory -C examples/appMaster clean
	@echo "Context cleaned"

install: all
	@echo "Installing include files in ${INSTALL_INCLUDE}${TARGET}"
	@if [ -d "${INSTALL_INCLUDE}${TARGET}" ]; then \
		echo "${BOLD}${RED}! Folder ${INSTALL_INCLUDE}${TARGET} already exist ! Moving it to ${INSTALL_INCLUDE}${TARGET}.bak${RESET}"; \
		mv ${INSTALL_INCLUDE}${TARGET} ${INSTALL_INCLUDE}${TARGET}.bak; \
	fi
	@mkdir -p ${INSTALL_INCLUDE}${TARGET}
	@cp -r include/Yarn++ ${INSTALL_INCLUDE}
	@echo "Installing library in ${INSTALL_LIBS}"
	@if [ -f "${INSTALL_LIBS}" ]; then \
		echo "${BOLD}${RED}! Folder ${INSTALL_LIBS} already exist ! Moving it to ${INSTALL_LIBS}.bak${RESET}"; \
		mv ${INSTALL_LIBS} ${INSTALL_LIBS}.bak; \
	fi
	@cp $(OUTPUT) ${INSTALL_LIBS}
