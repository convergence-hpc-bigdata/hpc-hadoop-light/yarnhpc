#ifndef YARNPP_UTILS_HPP
#define YARNPP_UTILS_HPP

#include <google/protobuf/stubs/common.h>
#include <google/protobuf/message.h>
#include <string>

using namespace std;

namespace Yarnpp
{
    /**
     * @brief Print bytes hexdump -C style
     * 
     * @param bytes Bytes to print
     * @param len   Length of line
     */
    void FormatBytes(const void *bytes, size_t len);
    /**
     * @brief Read protobuf varint32
     * 
     * @param p Buffer pointer
     * @return google::protobuf::uint32 Number read
     */
    google::protobuf::uint32 ReadVarint32(const char **p);
    /**
     * @brief Base64 encode string
     * 
     * @param in            String to encode
     * @return std::string  Encoded string
     */
    std::string base64_encode(const std::string &in);
    /**
     * @brief Base64 decode string
     * 
     * @param in            String to decode
     * @return std::string  Decoded string
     */
    std::string base64_decode(const std::string &in);
    /**
     * @brief Print a protobuf message
     */
    void printProtobufMessage(const ::google::protobuf::Message* msg, string tab = "");
}

#endif