#ifndef YARNPP_RPCBUFFEREDREADER
#define YARNPP_RPCBUFFEREDREADER

#include <cstddef>

namespace Yarnpp
{
    /**
     * @brief Buffered reader for protobuf over socket
     * 
     */
    class RpcBufferedReader
    {
    private:
        const int MAX_READ_ATTEMPTS = 100;
        size_t bufferLength = 100;
        size_t bufferRead = 0;
        char *buffer = NULL;
        int sockfd;
        size_t pos;

        long long verbose;

    public:
        /**
         * @brief Construct a new Rpc Buffered Reader object
         * 
         * @param socket Socket id
         */
        RpcBufferedReader(int socket);
        /**
         * @brief Destroy the Rpc Buffered Reader object
         * 
         */
        ~RpcBufferedReader();

        /**
         * @brief Read at most n chars
         * 
         * @param n      Number of chars to read
         * @return char* Result
         */
        char *read(size_t n);
        /**
         * @brief Rewind in the buffer
         * 
         * @param places Number of chars to go back to
         */
        void rewind(size_t places);

        /**
         * @brief Set the verbosity of the reader
         * 
         * @param verbose Verbose level
         */
        void setVerbosity(long long verbose);

    private:
        size_t _buffer_bytes(size_t n);
        void in(const void *bytes, size_t len);
        void reset();
    };

}

#endif