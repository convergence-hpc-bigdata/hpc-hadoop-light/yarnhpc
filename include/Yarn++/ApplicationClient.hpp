#ifndef YARNPP_APPLICATIONCLIENT_HPP
#define YARNPP_APPLICATIONCLIENT_HPP

#include <Yarn++/SocketRpcController.hpp>
#include <Yarn++/SocketRpcChannel.hpp>
#include <vector>
#include <string>
#include <map>

// Protobuf
#include <Yarn++/genprotos/applicationclient_protocol.pb.h>
#include <Yarn++/genprotos/yarn_protos.pb.h>

using namespace std;

namespace Yarnpp
{

    /**
     * @brief Application submit context
     * 
     * Shortcut for ApplicationSubmissionContextProto message from Hadoop:
     * message ApplicationSubmissionContextProto {
     *   optional ApplicationIdProto application_id = 1;
     *   optional string application_name = 2 [default = "N/A"];
     *   optional string queue = 3 [default = "default"];
     *   optional PriorityProto priority = 4;
     *   optional ContainerLaunchContextProto am_container_spec = 5;
     *   optional bool cancel_tokens_when_complete = 6 [default = true];
     *   optional bool unmanaged_am = 7 [default = false];
     *   optional int32 maxAppAttempts = 8 [default = 0];
     *   optional ResourceProto resource = 9;
     *   optional string applicationType = 10 [default = "YARN"];
     *   optional bool keep_containers_across_application_attempts = 11 [default = false];
     *   repeated string applicationTags = 12;
     *   optional int64 attempt_failures_validity_interval = 13 [default = -1];
     *   optional LogAggregationContextProto log_aggregation_context = 14;
     *   optional ReservationIdProto reservation_id = 15;
     *   optional string node_label_expression = 16;
     *   repeated ResourceRequestProto am_container_resource_request = 17;
     *   repeated ApplicationTimeoutMapProto application_timeouts = 18;
     *   repeated StringStringMapProto application_scheduling_properties = 19;
     * }
     * 
     */
    typedef hadoop::yarn::ApplicationSubmissionContextProto ApplicationContext;
    /**
     * @brief Application report structure
     * 
     * Shortcut for ApplicationReportProto message from Hadoop:
     * message ApplicationReportProto {
     *   optional ApplicationIdProto applicationId = 1;
     *   optional string user = 2;
     *   optional string queue = 3;
     *   optional string name = 4;
     *   optional string host = 5;
     *   optional int32 rpc_port = 6;
     *   optional hadoop.common.TokenProto client_to_am_token = 7;
     *   optional YarnApplicationStateProto yarn_application_state = 8;
     *   optional string trackingUrl = 9;
     *   optional string diagnostics = 10 [default = "N/A"];
     *   optional int64 startTime = 11;
     *   optional int64 finishTime = 12;
     *   optional FinalApplicationStatusProto final_application_status = 13;
     *   optional ApplicationResourceUsageReportProto app_resource_Usage = 14;
     *   optional string originalTrackingUrl = 15;
     *   optional ApplicationAttemptIdProto currentApplicationAttemptId = 16;
     *   optional float progress = 17;
     *   optional string applicationType = 18;
     *   optional hadoop.common.TokenProto am_rm_token = 19;
     *   repeated string applicationTags = 20;
     *   optional LogAggregationStatusProto log_aggregation_status = 21;
     *   optional bool unmanaged_application = 22 [default = false];
     *   optional PriorityProto priority = 23;
     *   optional string appNodeLabelExpression = 24;
     *   optional string amNodeLabelExpression = 25;
     *   repeated AppTimeoutsMapProto appTimeouts = 26;
     * }
     *
     */
    typedef hadoop::yarn::ApplicationReportProto ApplicationReport;
    /**
     * @brief Resource structure
     * 
     * Shortcut for ResourceProto message from Hadoop:
     * message ResourceProto {
     *   optional int64 memory = 1;
     *   optional int32 virtual_cores = 2;
     *   repeated ResourceInformationProto resource_value_map = 3;
     * }
     * 
     */
    typedef hadoop::yarn::ResourceProto Resources;

    /**
     * @brief Application client messaging class
     * 
     * This class allows to discuss with the Yarn application client protobuf server.
     * 
     */
    class ApplicationClient
    {
    public:
        /**
         * @brief Construct a new Application Client object
         * 
         * @param host      Hostname of the Yarn application client server
         * @param port      Port of the Yarn application client server
         * @param user      Username to use during the whole discussion
         * @param verbosity Verbosity level of the message exchange
         */
        ApplicationClient(string host, int port, string user, int verbosity = 0);
        /**
         * @brief Destroy the Application Client object
         * 
         */
        ~ApplicationClient();

        /**** Apps ****/
        /**
         * Common use of this class is described in the following steps:
         * 1. Create application and get ID of it
         * 2. Get capabilities associated to application
         * 3. Submit it with all parameters
         * 4. Change its priority
         * 5. Get or print reports
         * 6. Kill it
         */

        /**
         * @brief Get an Application identifier from Hadoop
         * 
         * @return int32_t ID of the new application
         */
        int32_t createApplication();
        /**
         * @brief Get the associated capabilities of a specific application
         * 
         * @param appId      Application ID
         * @return Resources Resources structure of the capabilities
         */
        Resources getAssociatedCapabilities(int32_t appId);
        /**
         * @brief Submit application to the Yarn cluster
         * 
         * @param appId             Application ID to submit
         * @param name              Name of the application
         * @param memory            Memory needed
         * @param vcores            Number of virtual cores needed
         * @param appMasterCommand  Command to launch the application master
         * @param envVars           Environment variables to add to application master runtime
         */
        void submitApplication(int32_t appId, string name, int64_t memory, int32_t vcores, string appMasterCommand, vector<pair<string, string>> envVars = vector<pair<string, string>>());
        /**
         * @brief Change an application priority
         * 
         * @param appId         Application ID
         * @param newPriority   Priority to set
         * @return int32_t      New priority of the application
         */
        int32_t changeApplicationPriority(int32_t appId, int32_t newPriority);
        /**
         * @brief Get the report of an application
         * 
         * @param appId              Application ID to get report from
         * @return ApplicationReport Report structure containing all the information concerning this application
         */
        ApplicationReport getReport(int32_t appId);
        /**
         * @brief Print report to stdout
         * 
         * @param appId Application ID to get report from
         */
        void printReport(int32_t appId);
        /**
         * @brief Force kill an application
         * 
         * @param appId  ID of the application to kill
         * @return true  Application killed
         * @return false Application remains alive
         */
        bool deleteApplication(int32_t appId);

        /**** Status ****/
        /**
         * @brief Print the status of this application client
         * 
         */
        void printStatus();
        /**
         * @brief Returns if an error happened or not
         * 
         * @return true  An error happened
         * @return false Everything fine
         */
        bool hasError();
        /**
         * @brief Get the error message 
         * 
         * @return string Error message
         */
        string getError();

    private:
        /**
         * @brief Clear error of this client
         * 
         */
        void clearError();
        /**
         * @brief Set error for this client
         * 
         * @param err Error message
         */
        void putError(string err);

    private:
        hadoop::yarn::ApplicationClientProtocolService::Stub *service;
        SocketRpcController *controller;
        SocketRpcChannel *channel;

        map<int32_t, hadoop::yarn::GetNewApplicationResponseProto> apps;

        bool hasErrorSet;
        string error;

        const char DEFAULT_YARN_PROTOCOL_VERSION = 0x9;
    };

}

#endif