#ifndef YARNPP_APPLICATIONMASTER_HPP
#define YARNPP_APPLICATIONMASTER_HPP

#include <Yarn++/SocketRpcController.hpp>
#include <Yarn++/SocketRpcChannel.hpp>
#include <Yarn++/ContainerManager.hpp>
#include <Yarn++/Credentials.hpp>
#include <string>
#include <vector>

// Protobuf
#include <Yarn++/genprotos/applicationmaster_protocol.pb.h>

using namespace std;

namespace Yarnpp
{
    /**
     * @brief Possible final application status
     * 
     */
    namespace ApplicationStatus
    {
        hadoop::yarn::FinalApplicationStatusProto APP_UNDEFINED = hadoop::yarn::FinalApplicationStatusProto::APP_UNDEFINED;
        hadoop::yarn::FinalApplicationStatusProto APP_SUCCEEDED = hadoop::yarn::FinalApplicationStatusProto::APP_SUCCEEDED;
        hadoop::yarn::FinalApplicationStatusProto APP_FAILED = hadoop::yarn::FinalApplicationStatusProto::APP_FAILED;
        hadoop::yarn::FinalApplicationStatusProto APP_KILLED = hadoop::yarn::FinalApplicationStatusProto::APP_KILLED;
        hadoop::yarn::FinalApplicationStatusProto APP_ENDED = hadoop::yarn::FinalApplicationStatusProto::APP_ENDED;
    }

    /**
     * @brief Application master messaging class
     * 
     * This class allows to discuss with the Yarn application master protobuf server.
     * 
     */
    class ApplicationMaster
    {
    public:
        /**
         * @brief Construct a new Application Master object
         * 
         * @param host      Hostname of the yarn application master server
         * @param port      Port of the yarn application master server
         * @param verbosity Verbosity level of this application master
         */
        ApplicationMaster(string host, int port, int verbosity = 0);
        /**
         * @brief Destroy the Application Master object
         * 
         */
        ~ApplicationMaster();

        /**
         * @brief Register this application in Yarn application manager
         * 
         */
        void registerApplicationMaster();
        /**
         * @brief Allocate a container to do a job
         * 
         */
        void allocate(int64_t memory, int32_t vcores, string command);
        /**
         * @brief Finish this application in Yarn application manager
         * 
         * @param finalStatus Final status of the application
         */
        void finishApplicationMaster(hadoop::yarn::FinalApplicationStatusProto finalStatus);

    private:
        /**
         * @brief Set error message
         * 
         * @param error Error message
         */
        void putError(string error);
        /**
         * @brief Clear the error in the application master
         * 
         */
        void clearError();
        /**
         * @brief Parse the token file to retrieve different tokens
         * 
         */
        void parseTokenFile();

    private:
        hadoop::yarn::ApplicationMasterProtocolService::Stub *service;
        SocketRpcController *controller;
        SocketRpcChannel *channel;

        Credentials creds;
        string effectiveUser;
        string username;
        string password;
        int verbosity;

        bool hasError;
        string error;

        const char DEFAULT_YARN_PROTOCOL_VERSION = 0x9;

        vector<ContainerManager*> managers;
    };

}

#endif