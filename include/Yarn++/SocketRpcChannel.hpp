#ifndef YARNPP_SOCKETRPCCHANNEL_HPP
#define YARNPP_SOCKETRPCCHANNEL_HPP

#include <Yarn++/genprotos/RpcHeader.pb.h>

#include <Yarn++/RpcBufferedReader.hpp>
#include <google/protobuf/service.h>
#include <google/protobuf/message.h>
#include <string>

using namespace std;

namespace Yarnpp
{
    /**
     * @brief Protobuf Rpc channel over socket
     * 
     */
    class SocketRpcChannel : public google::protobuf::RpcChannel
    {
    private:
        const unsigned long long ERROR_BYTES = 18446744073709551615ULL;
        const string RPC_HEADER = "hrpc";
        const char RPC_SERVICE_CLASS = 0x00;
        const char AUTH_PROTOCOL_NONE = 0x00;
        const char AUTH_PROTOCOL_SASL = 0xDF;
        const char RPC_PROTOCOL_BUFFER = 0x02;

        string host;
        int port;
        int sock;
        // First time (when the connection context is sent, the call_id should be -3, otherwise start with 0 and increment)
        int callID;
        char version;
        string clientID;
        bool useSasl;
        string contextProtocol;
        string effectiveUser;
        int sockConnectTimeout;
        int sockRequestTimeout;

        long long verbose = 0;

        char *username;
        char *password;

    public:
        /**
         * @brief Construct a new Socket Rpc Channel object
         * 
         * @param host               Hostname to connect to
         * @param port               Port to connect to
         * @param version            Version of hadoop protobuf
         * @param contextProcotol    Context protocol to use
         * @param effectiveUser      Effective username during messaging
         * @param useSasl            Sasl enabled or not
         * @param krbPrincipal       Kerberos principal string
         * @param sockConnectTimeout Connection timeout time
         * @param sockRequestTimeout Socket request timeout time
         */
        SocketRpcChannel(string host, int port, char version, string contextProcotol, string effectiveUser = "", bool useSasl = false, bool krbPrincipal = false, int sockConnectTimeout = 10000, int sockRequestTimeout = 10000);
        /**
         * @brief Destroy the Socket Rpc Channel object
         * 
         */
        ~SocketRpcChannel();

        /**
         * @brief Set the verbosity level
         * 
         * @param level verbosity level
         */
        void setVerbosity(long long level);
        /**
         * @brief Set the username for authentication
         * 
         * @param username
         */
        void setUsername(const char *username);
        /**
         * @brief Set the password for authentication
         * 
         * @param password 
         */
        void setPassword(const char *password);

    private:
        void validateRequest(const google::protobuf::Message *request);
        void getConnection();
        void connectSasl();
        void write(const void *data, size_t len);
        void writeDelimited(const void *data, size_t len);
        string createRpcRequestHeader();
        string createConnectionContext();
        void sendRpcMessage(const google::protobuf::MethodDescriptor *method, const google::protobuf::Message *request);
        void sendSaslMessage(const google::protobuf::Message *msg);
        string createRequestHeader(const google::protobuf::MethodDescriptor *method);
        RpcBufferedReader recvRpcMessage();
        hadoop::common::RpcSaslProto recvSaslMessage();
        int getLength(RpcBufferedReader *stream);
        int parseResponse(RpcBufferedReader *stream, google::protobuf::Message *response);
        void parseExtraMessage(google::protobuf::Message *response);
        void handleError(hadoop::common::RpcResponseHeaderProto header);
        void closeSocket();

        void CallMethod(const google::protobuf::MethodDescriptor *method, google::protobuf::RpcController *controller, const google::protobuf::Message *request, google::protobuf::Message *response, google::protobuf::Closure *done);

        void out(const void *bytes, size_t len);
    };

}

#endif