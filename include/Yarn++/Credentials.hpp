#ifndef CREDENTIALS_HPP
#define CREDENTIALS_HPP

#include <Yarn++/genprotos/Security.pb.h>

#include <fstream>
#include <map>

using namespace std;

namespace Yarnpp
{
    /**
     * @brief Credentials storage for application master
     * 
     * Class used to read token storage file within application master
     * 
     */
    class Credentials
    {
    public:
        const char WRITABLE = 0x0;
        const char PROTOBUF = 0x0;

        const int TOKEN_STORAGE_MAGIC_LEN = 4;
        const char *TOKEN_STORAGE_MAGIC = "HDTS";

    public:
        /**
         * @brief Construct a new Credentials object
         * 
         */
        Credentials();
        /**
         * @brief Destroy the Credentials object
         * 
         */
        ~Credentials();

        /**
         * @brief Read the token storage file
         * 
         * @param filename Path to the token storage file
         */
        void readTokenStorageFile(const char *filename);

        /**
         * @brief Get a token from its key
         * 
         * @param key                         Key of the token (can be "")
         * @return hadoop::common::TokenProto Associated token
         */
        hadoop::common::TokenProto getToken(string key);
        /**
         * @brief Get a secret from its key
         * 
         * @param key                Key of the secret
         * @return pair<int, char *> Associated secret
         */
        pair<int, char *> getSecret(string key);

    private:
        void readFields(ifstream &ifs);
        void readProto(ifstream &ifs);

        int readVInt(ifstream &ifs);

    private:
        map<string, hadoop::common::TokenProto> tokens;
        map<string, pair<int, char *>> secrets;
    };

}

#endif