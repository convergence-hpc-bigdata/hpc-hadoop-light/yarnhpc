#ifndef VERBOSITY_HPP
#define VERBOSITY_HPP

namespace Yarnpp
{
    namespace Verbose {
        const long long Packets = 1;
        const long long Resources = Packets << 1;

        void listVerbosityCodes();
    }
}

#endif