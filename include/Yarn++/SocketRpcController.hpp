#ifndef YARNPP_CONTROLLER_H
#define YARNPP_CONTROLLER_H

#include <google/protobuf/service.h>
#include <string>

using namespace std;

namespace Yarnpp
{
    /**
     * @brief Protobuf controller for socket
     * 
     */
    class SocketRpcController : public google::protobuf::RpcController
    {
    private:
        string error;

    public:
        ~SocketRpcController();

        void Reset();
        bool Failed() const;
        string ErrorText() const;
        void StartCancel();

        void SetFailed(const string &reason);
        bool IsCanceled() const;
        void NotifyOnCancel(google::protobuf::Closure *callback);
    };

}

#endif