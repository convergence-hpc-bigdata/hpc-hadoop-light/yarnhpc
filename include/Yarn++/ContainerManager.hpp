#ifndef YARNPP_CONTAINERMANAGER_HPP
#define YARNPP_CONTAINERMANAGER_HPP

#include <Yarn++/SocketRpcController.hpp>
#include <Yarn++/SocketRpcChannel.hpp>
#include <Yarn++/Credentials.hpp>
#include <string>

// Protobuf
#include <Yarn++/genprotos/containermanagement_protocol.pb.h>

using namespace std;

namespace Yarnpp
{
    /**
     * @brief Container manager messaging class
     * 
     * This class allows to discuss with the Yarn container manager protobuf server.
     * 
     */
    class ContainerManager
    {
    public:
        /**
         * @brief Construct a new Container Manager object
         * 
         * @param host      Hostname of the yarn application master server
         * @param port      Port of the yarn application master server
         * @param verbosity Verbosity level of this application master
         */
        ContainerManager(string host, int port, hadoop::common::TokenProto nodeManagerToken, hadoop::common::TokenProto containerToken, string effectiveUser, int verbosity = 0);
        /**
         * @brief Destroy the Container Manager object
         * 
         */
        ~ContainerManager();

        /**
         * @brief Start the container
         */
        void startContainer(string command);
        /**
         * @brief Stop the container
         */
        void stopContainer();

    private:
        void parseToken();

    private:
        hadoop::yarn::ContainerManagementProtocolService::Stub *service;
        SocketRpcController *controller;
        SocketRpcChannel *channel;

        string username;
        string password;
        string effectiveUser;
        int containerId;
        const char DEFAULT_YARN_PROTOCOL_VERSION = 0x9;

        hadoop::common::TokenProto nodeManagerToken;
        hadoop::common::TokenProto containerToken;
    };

}

#endif